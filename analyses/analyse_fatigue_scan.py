"""
Script will fit the abaqus data and the medusa data for each iteration. The error between the two will be measured.
"""
import h5py as h5
import glob
import scipy as sp
import scipy.interpolate
import scipy.io
import numpy as np

def get_vms(sxx, syy, sxy, nu):
    szz = nu * (sxx+syy)
    return np.sqrt(np.power(sxx, 2)+3*np.power(sxy, 2)+np.power(syy,
                                                                2)+(-1)*syy*szz+np.power(szz, 2)+(-1)*sxx*(syy+szz))

# Load Abaqus
data = scipy.io.loadmat('../data/final/fwo_abaqus.mat')
y_abaqus = data['sxx_aba'][0]
x_abaqus = data['x_aba'][0] * 1000

new_x = np.linspace(-0.5, 0.5, 1000)
y_abaqus = sp.interpolate.interp1d(x_abaqus, y_abaqus, kind='linear')(new_x)

# Run over all Medusa files.
all_files = glob.glob("..\\data\\fatigue_param_scan\\*.h5")
failed_files = []
stats = {}
for file in all_files:
    try:
        data_raw = h5.File(file, 'r')

        N_iter = data_raw.attrs['refinement_iterations']
        err_per_iteration = []
        for iteration in range(N_iter):
            pos = np.array(data_raw['domain_{:05d}/pos'.format(iteration)][:]) * 1000
            stress = data_raw['stress_implicit_{:05d}'.format(iteration)][:]
            sxx = stress[0] * 1e-6

            y_medusa = sp.interpolate.interp1d(pos[0], sxx, kind='linear')(new_x)

            err = np.abs(y_medusa - y_abaqus)
            err_per_iteration.append(np.mean(err))
        
        # Find minimum error.
        err_min = np.min(err_per_iteration)
        idx_min = err_per_iteration.index(err_min)

        # Store data.
        temp = {}
        temp['err'] = err_min
        temp['iteration'] = idx_min
        stats['{}'.format(file)] = temp
    except:
        failed_files.append(file)
    data_raw.close()
sorted_keys = sorted(stats, key=lambda x: (stats[x]['err']))
best_keys = sorted_keys[:10]

best_stats = {}
for key in best_keys:
    best_stats[key] = stats[key]

print(best_stats)