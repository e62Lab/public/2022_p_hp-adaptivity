import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import h5py as h5
import glob
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy.sparse as sparse
import scipy.linalg as linalg
import itertools

from scipy import linalg as LA
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MaxNLocator
from scipy.sparse import coo_matrix

import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import json
import matplotlib.gridspec as gridspec


dim = 2
has_all_data = True
data_filepaths = [
  ['../data/final/timed/results_boussinesq_solver_B_G_P.h5'],
  ['../data/final/timed/results_boussinesq_solver_B_G.h5'],
  # ['../data/final/timed/results_boussinesq_solver_B_P.h5'],
  # ['../data/final/timed/results_boussinesq_solver_B.h5'],
  ['../data/final/timed/results_boussinesq_solver_PU.h5'],
  ['../data/final/timed/results_boussinesq_solver_LU.h5'],
]

solver_names = ['BiCGSTAB + ILUT',
                'BiCGSTAB',
                # 'BiCGSTAB + ILUT',
                # 'BiCGSTAB',
                'PardisoLU',
                'SparseLU',
                # 'SparseQR',
                ]

print('WARNING: can take up to 30 minutes due to large matrices.')
data_filepath = '../data/final/timed/results_boussinesq_solver_B_G_P.h5'
output_file_postfix = '_eigenvalues'
out_file = '../data/final/' + data_filepath.split('/')[-1].split('.h5')[0] + output_file_postfix + '.json'
iterations = [3, 11, 23]

eigen_data = {}
# compute new eigenvectors.

if 1:
    with h5.File(data_filepath, "r") as dataFile:
        for iteration in iterations:
            print(iteration)
            i, j, val = dataFile["Matrix_{:05d}".format(iteration)][()]
            i = (i-1).astype(int)
            j = (j-1).astype(int)
            mat = sparse.coo_matrix((val, (i, j)))
            # better eigenvalue algorithms available for dense matrices
            dense = mat.toarray()
            eVal, eVec = linalg.eig(dense)
            serializable_eVal = [[e.real, e.imag] for e in eVal]
            eigen_data['{}'.format(iteration)] = serializable_eVal
        
        # Save eigenvalues to file.
        with open(out_file, "w") as write_file:
            json.dump(eigen_data, write_file, indent=2)
else:
    #import
    with open(out_file) as dataFile:
        eigen_data = json.load(dataFile)