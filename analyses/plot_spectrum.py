import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import h5py as h5
import glob
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy.sparse as sparse
import scipy.linalg as linalg
import itertools

from scipy import linalg as LA
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MaxNLocator
from scipy.sparse import coo_matrix

import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import json
import matplotlib.gridspec as gridspec
import scipy.sparse as sparse

from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

plt.rcParams.update({
    "mathtext.fontset": "stix",
    "font.family": "STIXGeneral",
    "text.usetex": False,
    "axes.formatter.use_mathtext": True,
    "xtick.labelsize": 12,
    "ytick.labelsize": 12,
    "axes.labelsize": 14
})


print('WARNING: can take long due to large matrices.')
data_filepath = '../data/results_boussinesq_solver_B_P_G.h5'
output_file_postfix = '_eigenvalues'
out_file = '../data/final/' + data_filepath.split('/')[-1].split('.h5')[0] + output_file_postfix + '.json'
iterations = [0, 5, 11]

eigen_data = {}
# compute new eigenvectors.

if 0:
    with h5.File(data_filepath, "r") as dataFile:
        for iteration in iterations:
            print(iteration)
            i, j, val = dataFile["Matrix_{:05d}".format(iteration)][()]
            i = (i-1).astype(int)
            j = (j-1).astype(int)
            mat = sparse.coo_matrix((val, (i, j)))
            # better eigenvalue algorithms available for dense matrices
            # dense = mat.toarray()
            # ‘LM’ : largest magnitude
            # ‘SM’ : smallest magnitude
            # ‘LR’ : largest real part
            # ‘SR’ : smallest real part
            # ‘LI’ : largest imaginary part
            # ‘SI’ : smallest imaginary part
            eVal=sparse.linalg.eigs(mat, k=50, which='LM', return_eigenvectors=False)
            serializable_eVal = [[e.real, e.imag] for e in eVal]
            eigen_data['{}'.format(iteration)] = serializable_eVal
        
        # Save eigenvalues to file.
        with open(out_file, "w") as write_file:
            json.dump(eigen_data, write_file, indent=2)
else:
    #import
    for iteration in iterations:
        with open(f'../data/final/results_boussinesq_solver_B_P_G_eigenvalues_{iteration}.json') as dataFile:
            d = json.load(dataFile)
            eigen_data[str(iteration)] = d[str(iteration)]
    

print('WARNING: Can take up to 10 minutes due to large matrices!')

with h5.File(data_filepath, "r") as dataFile:
    fig = plt.figure(figsize=(9,5), tight_layout=True)
    gs = gridspec.GridSpec(2, 3, height_ratios=[1,0.5])

    for col_idx, iteration in enumerate(iterations):
        print('iteration', iteration)
        ax = fig.add_subplot(gs[0, col_idx])

        M = dataFile["Matrix_{:05d}".format(iteration)][:] # array of triplets!
        dataType = dataFile['domain_{:05d}/types'.format(iteration)][:] # Node type (boundary or interior)
        # convert triplets to matrix
        N = 3*len(dataType)
        row = M[0,:] - 1
        col = M[1,:] - 1
        val = M[2,:]
        matrix = coo_matrix((val, (row, col)), shape = (N, N)).toarray()
        M = matrix
        ax.spy(M, ms = 0.01, marker = '.', c = 'black')
        ax.grid()
        
        
        ax.set_title(f'iteration = {iteration}, $N={len(dataType)}$')
        ticks = np.linspace(min(row), max(row), 8)
        ticks = [int(t) for t in ticks]
        ax.xaxis.set_major_locator(MultipleLocator(10000 if col_idx < 2 else 30000))

        ax.xaxis.set_minor_locator(MultipleLocator(5000 if col_idx < 2 else 15000))
        # ax.set_xticklabels([])
        ax.yaxis.set_major_locator(MultipleLocator(10000 if col_idx < 2 else 30000))
        ax.yaxis.set_minor_locator(MultipleLocator(5000 if col_idx < 2 else 15000))
        # ax.set_yticklabels([])

        ax = fig.add_subplot(gs[1, col_idx])
        eVal = [complex(val[0] + 1j * val[1]) for val in eigen_data['{}'.format(iteration)]]
        ax.plot(np.real(eVal), np.imag(eVal), 'ko', markerfacecolor="none", ms=2)
        ax.ticklabel_format(axis='both', style='sci', scilimits=(0,0))
        ax.set_xlabel('$\Re (\lambda)$')
        ax.grid()
        if (col_idx == 0):
            ax.set_ylabel('$\Im (\lambda)$')

fig.tight_layout()
if 1:
    plt.savefig('../manuscript/figures/spectra_bou.png', dpi=300, transparent=False, bbox_inches='tight')
plt.show()