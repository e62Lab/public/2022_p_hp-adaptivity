import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import h5py as h5
import glob


all_files = glob.glob("..\\data\\boussinesq_scan\\*.h5")

failed_files = []
stats = {}
N = 0
for file in all_files:
    file_stats = {}
    try:
        # Load.
        data_raw = h5.File(file, 'r')
        # data = data_raw['1']
        data = data_raw

        # Get number of iterations.
        N_iter = data.attrs['refinement_iterations']
        file_stats['iterations'] = N_iter

        # Get infinity norm error list.
        error_implicit = [
            data.attrs['err_norm_inf_{:05d}'.format(i)] for i in range(N_iter)]
        file_stats['min_error'] = min(error_implicit)
        step_with_min_error = error_implicit.index(min(error_implicit))
        file_stats['min_error_step'] = step_with_min_error

        # Get params set.
        file_stats['h_delta1'] = data['conf'].attrs['adaptivity.h.delta1']
        file_stats['h_delta2'] = data['conf'].attrs['adaptivity.h.delta2']
        file_stats['h_refine'] = data['conf'].attrs['adaptivity.h.agressiveness']
        file_stats['h_derefine'] = data['conf'].attrs['adaptivity.derefine.h.agressiveness']
        file_stats['p_delta1'] = data['conf'].attrs['adaptivity.p.delta1']
        file_stats['p_delta2'] = data['conf'].attrs['adaptivity.p.delta2']
        file_stats['p_refine'] = data['conf'].attrs['adaptivity.p.agressiveness']
        file_stats['p_derefine'] = data['conf'].attrs['adaptivity.derefine.p.agressiveness']
        file_stats['number_of_p_derefined'] = [
            data.attrs['order_derefined_{:05d}'.format(i)] for i in range(N_iter)]

        # Domain info.
        file_stats['final_domain_size'] = data.attrs['N_{:05d}'.format(
            N_iter-1)]

        # Store stats.
        stats[file] = file_stats

        # Stats.
        N += 1

        # Close.
        data_raw.close()
    except:
        failed_files.append(file)
print("Read: ", N)
print("Failed: ", len(failed_files))

# Get best N files.
N = 5
best = sorted(stats, key=lambda x: (stats[x]['min_error'], stats[x]['number_of_p_derefined']))[:N]
for b in best:
    print(b)
    print(stats[b])
