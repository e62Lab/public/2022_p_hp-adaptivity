# Raw data

Raw results from simulations.

## Download data

Or download data from [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7639648.svg)](https://doi.org/10.5281/zenodo.7639648).