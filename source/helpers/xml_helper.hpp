#ifndef XML_HELPER_HPP
#define XML_HELPER_HPP

#include <medusa/Medusa_fwd.hpp>
#include "math_helper.hpp"

using namespace mm;
using namespace std;

/**
 * Creates a range of XML inputs for easier computation.
 * @param conf Master input XML.
 * @return Range of XML objects.
 */
Range<XML> get_xml_range(const XML& conf) {
    if (conf.get<int>("debug.print") == 1) {
        cout << "Creating XML range ..." << endl;
    }

    // Initialize list of output XMLS.
    Range<XML> all_conf;

    Range<string> nodes = split(conf.get<string>("discretization.nodes_per_side"), ',');
    Range<string> mon_degrees = split(conf.get<string>("approx.mon_degree"), ',');
    auto N_runs = conf.get<int>("case.n_runs");
    Range<string> alphas = split(conf.get<string>("case.alpha"), ',');

    for (string alpha : alphas) {
        // Copy master configuration.
        XML _conf(conf);
        _conf.set("case.alpha", alpha, true);

        for (string node : nodes) {
            _conf.set("discretization.nodes_per_side", node, true);

            for (string mon_degree : mon_degrees) {
                _conf.set("approx.mon_degree", mon_degree, true);

                for (int i = 0; i < N_runs; i++) {
                    _conf.set("case.n_runs", 1, true);
                    // Store to range.
                    all_conf.push_back(_conf);
                }
            }
        }
    }

    if (conf.get<int>("debug.print") == 1) {
        cout << "XML range created." << endl;
    }

    return all_conf;
}

#endif  // XML_HELPER_HPP
