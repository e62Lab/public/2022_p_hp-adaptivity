#ifndef MATH_HELPER_HPP
#define MATH_HELPER_HPP

#include <algorithm>

using namespace std;

/**
 * @brief Returns value of Binomial Coefficient C(n, k).
 * https://www.geeksforgeeks.org/binomial-coefficient-dp-9/
 *
 * @param n
 * @param k
 * @return int
 */
int binomialCoeff(int n, int k) {
    int C[n + 1][k + 1];
    int i, j;

    // Calculate value of Binomial Coefficient in bottom up manner.
    for (i = 0; i <= n; i++) {
        for (j = 0; j <= min(i, k); j++) {
            // Base Cases.
            if (j == 0 || j == i) C[i][j] = 1;

            // Calculate value using previously stored values.
            else
                C[i][j] = C[i - 1][j - 1] + C[i - 1][j];
        }
    }

    return C[n][k];
}

/**
 * Sign function.
 * @tparam T
 * @param val
 * @return
 */
template <typename T>
int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

// https://stackoverflow.com/questions/3407012/rounding-up-to-the-nearest-multiple-of-a-number
int roundUp(double numToRound, int multiple) {
    return (int)(std::roundf(numToRound / multiple) * multiple + 0.5);
}

double get_deviation(const mm::Range<int>& ind, const Eigen::VectorXd& solution) {
    double mu = 0;
    for (int i : ind) {
        mu += solution[i];
    }
    mu /= ind.size();

    double var = 0;
    for (int i : ind) {
        var += (solution[i] - mu) * (solution[i] - mu);
    }
    return std::sqrt(var);
}

template <typename Derived>
typename Derived::Scalar median(Eigen::DenseBase<Derived>& d) {
    auto r{d.reshaped()};
    std::sort(r.begin(), r.end());
    return r.size() % 2 == 0 ? r.segment((r.size() - 2) / 2, 2).mean() : r(r.size() / 2);
}

#endif  // MATH_HELPER_HPP
