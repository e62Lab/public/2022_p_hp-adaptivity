#ifndef P_ADAPTIVE_FWO_HP_ADAPTIVE_H
#define P_ADAPTIVE_FWO_HP_ADAPTIVE_H

#include <medusa/Medusa.hpp>
#include "fwo_implicit.h"
#include "fwo_explicit.h"
#include "domain_helper.h"
#include "modify_order.h"

using namespace std;

void run_fwo_hp_adaptive(const mm::XML& conf, mm::HDF& hdf) {
    // Timer initialization.
    mm::Timer timer;
    timer.addCheckPoint("start");

    // Read booleans from input.
    const bool debug = conf.get<bool>("debug.print");
    const bool save_all = conf.get<bool>("meta.all_data");

    // Build domain.
    if (debug) {
        std::cout << "Building domain ..." << std::endl;
    }
    timer.addCheckPoint("domain");
    double dx = conf.get<double>("num.dx");  // Discretization step.
    mm::DomainDiscretization<mm::Vec2d> domain =
        make_domain(conf, [=](const mm::Vec2d&) { return dx; });
    timer.addCheckPoint("domain_created");

    // Engines.
    mm::Range<mm::RBFFD<mm::Polyharmonic<double>, mm::Vec2d, mm::ScaleToClosest>> engines_implicit;
    mm::Range<mm::RBFFD<mm::Polyharmonic<double>, mm::Vec2d, mm::ScaleToClosest>> engines_explicit;
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;
    for (int i = min_order; i <= max_order + step; i += step) {
        mm::RBFFD<mm::Polyharmonic<double>, mm::Vec2d, mm::ScaleToClosest> engine(
            conf.get<int>("approx.phs_order"), i);
        if (i <= max_order) {
            engines_implicit.push_back(engine);
        }
        if (i > min_order) {
            engines_explicit.push_back(engine);
        }
    }

    // ******************************
    // Iterate hp-adaptive procedure.
    // ******************************
    int max_adaptivity_iterations = conf.get<int>("adaptivity.max_iterations");
    int adaptivity_iterations = 0;
    double min_indicator_reduction = conf.get<double>("adaptivity.stop.imex_reduction");
    double initial_indicator;

    do {
        timer.addCheckPoint(mm::format("iteration_%.5i_start", adaptivity_iterations));
        if (debug) {
            cout << "----------------" << endl;
            cout << "  Iteration id: " << adaptivity_iterations << endl;
            prn(domain.size());
        }
        // Save domain to HDF.
        if (save_all) {
            hdf.writeDomain(mm::format("domain_%.5i", adaptivity_iterations), domain);
        }
        // Save domain size to HDF.
        hdf.writeIntAttribute(mm::format("N_%.5i", adaptivity_iterations), domain.size());

        // Solve implicit.
        timer.addCheckPoint(mm::format("implicit_%.5i_start", adaptivity_iterations));
        mm::VectorField2d disp_implicit;    // Displacement vector field.
        mm::VectorField3d stress_implicit;  // Stress vector field.
        Eigen::VectorXd rhs_implicit;       // Implicit RHS.
        std::tie(disp_implicit, stress_implicit) =
            fwo_implicit(conf, hdf, domain, engines_implicit, rhs_implicit, adaptivity_iterations);
        // Save displacements and stresses to file.
        if (save_all) {
            hdf.writeEigen(mm::format("disp_implicit_%.5i", adaptivity_iterations), disp_implicit);
            hdf.writeEigen(mm::format("stress_implicit_%.5i", adaptivity_iterations),
                           stress_implicit);
        }
        timer.addCheckPoint(mm::format("implicit_%.5i_end", adaptivity_iterations));

        // IMEX indicator.
        timer.addCheckPoint(mm::format("imex_%.5i_start", adaptivity_iterations));
        Eigen::VectorXd disp_explicit = fwo_explicit(conf, domain, disp_implicit, engines_explicit);
        if (save_all) {
            hdf.writeEigen(mm::format("disp_explicit_%.5i", adaptivity_iterations), disp_explicit);
        }
        // Obtain IMEX vector field.
        auto indicator_vector_field =
            mm::VectorField2d::fromLinear((Eigen::VectorXd)(disp_explicit - rhs_implicit));
        // Compute IMEX as linear.
        Eigen::VectorXd indicator_field(domain.size());
        mm::Range<double> interior_values(domain.interior().size());
        int j = 0;
        for (int i : domain.interior()) {
            indicator_field[i] = indicator_vector_field[i].norm();
            interior_values[j] = indicator_field[i];
            j++;
        }
        // Compute IMEX on boundary.
        mm::SheppardInterpolant<mm::Vec2d, double> interpolant_error_indicator(
            domain.positions()[domain.interior()], interior_values);
        for (int i : domain.boundary()) {
            mm::Vec2d normal = domain.normal(i);
            indicator_field[i] = interpolant_error_indicator(domain.pos(i), 3);
        }

        timer.addCheckPoint(mm::format("imex_%.5i_end", adaptivity_iterations));

        // Write indicator field to file.
        if (save_all) {
            hdf.writeDoubleArray(mm::format("indicator_%.5i", adaptivity_iterations),
                                 indicator_field);
        }
        // Write max indicator value to file.
        hdf.writeDoubleAttribute(mm::format("max_indicator_%.5i", adaptivity_iterations),
                                 indicator_field.maxCoeff());

        // Recompute stopping criterion.
        double max_indicator = indicator_field.maxCoeff();
        // Store max initial indicator values.
        if (adaptivity_iterations == 0) {
            initial_indicator = max_indicator;
        }
        // Stopping criteria.
        double indicator_reduction = max_indicator / initial_indicator;
        if (debug) {
            prn(indicator_reduction);
        }
        if (indicator_reduction <= min_indicator_reduction) {
            mm::print_green(mm::format(
                "Convergence criteria reached. Indicator reduced by %f (required % f).\n ",
                indicator_reduction, min_indicator_reduction));
            timer.addCheckPoint(mm::format("iteration_%.5i_end", adaptivity_iterations));
            break;
        }

        // Apply adaptivity logic.
        timer.addCheckPoint(mm::format("refinement_%.5i_start", adaptivity_iterations));
        apply_adaptivity_logic(conf, domain, hdf, adaptivity_iterations, indicator_field);
        timer.addCheckPoint(mm::format("refinement_%.5i_end", adaptivity_iterations));

        // Next iteration.
        timer.addCheckPoint(mm::format("iteration_%.5i_end", adaptivity_iterations));
        adaptivity_iterations++;
    } while ((adaptivity_iterations < max_adaptivity_iterations));

    hdf.writeIntAttribute("refinement_iterations", adaptivity_iterations);
    if (adaptivity_iterations == max_adaptivity_iterations) {
        mm::print_red("Stopped by maximum iteration count.\n");
    }

    // Finish.
    timer.addCheckPoint("end");
    prn(timer.duration("start", "end"));
}

#endif  // P_ADAPTIVE_FWO_HP_ADAPTIVE_H
