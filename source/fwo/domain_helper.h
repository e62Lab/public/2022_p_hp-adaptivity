//
// Created by mitja on 27/07/2022.
//

#ifndef P_ADAPTIVE_DOMAIN_HELPER_H
#define P_ADAPTIVE_DOMAIN_HELPER_H

#include <medusa/Medusa.hpp>

template <typename func_t>
mm::DomainDiscretization<mm::Vec2d> make_domain(const mm::XML& conf, const func_t& dx) {
    const double width = conf.get<double>("case.width");
    const double height = conf.get<double>("case.height");
    mm::BoxShape<mm::Vec2d> shape({-width / 2, -height / 2}, {width / 2, 0});

    auto interior_type = conf.get<int>("num.interior");
    mm::DomainDiscretization<mm::Vec2d> domain =
        shape.discretizeBoundaryWithDensity(dx, -interior_type);

    mm::GeneralFill<mm::Vec2d> fill_randomized;
    int seed = conf.get<int>("num.fill_seed");
    if (seed == -1) {
        seed = mm::get_seed();
    }
    fill_randomized.seed(seed);
    fill_randomized(domain, dx, interior_type);

    // Remove corners.
    mm::Range<int> nodes_to_remove;
    for (int i : domain.boundary()) {
        auto pos = domain.pos(i);

        if ((pos - mm::Vec2d{-width / 2, 0}).norm() < 1e-6) {
            nodes_to_remove.push_back(i);
        }
        if ((pos - mm::Vec2d{width / 2, 0}).norm() < 1e-6) {
            nodes_to_remove.push_back(i);
        }
        if ((pos - mm::Vec2d{-width / 2, -height / 2}).norm() < 1e-6) {
            nodes_to_remove.push_back(i);
        }
        if ((pos - mm::Vec2d{width / 2, -height / 2}).norm() < 1e-6) {
            nodes_to_remove.push_back(i);
        }
    }

    // Remove.
    domain.removeNodes(nodes_to_remove);

    return domain;
}

int get_boundary_id(mm::Vec2d normal) {
    int id;

    if ((normal - mm::Vec2d{0, 1}).norm() < 1e-6) {
        // TOP
        id = -4;
    }
    if ((normal - mm::Vec2d{1, 0}).norm() < 1e-6) {
        // RIGHT
        id = -2;
    }
    if ((normal - mm::Vec2d{0, -1}).norm() < 1e-6) {
        // BOTTOM
        id = -3;
    }
    if ((normal - mm::Vec2d{-1, 0}).norm() < 1e-6) {
        // LEFT
        id = -1;
    }
    if ((normal(0) < 0) && (normal(1) > 0)) {
        // TOP
        id = -4;
    }
    if ((normal(0) > 0) && (normal(1) > 0)) {
        // RIGHT
        id = -2;
    }
    if ((normal(0) > 0) && (normal(1) < 0)) {
        // BOTTOM
        id = -3;
    }
    if ((normal(0) < 0) && (normal(1) < 0)) {
        // LEFT
        id = -1;
    }

    return id;
}

#endif  // P_ADAPTIVE_DOMAIN_HELPER_H
