//
// Created by mitja on 27/07/2022.
//

#ifndef P_ADAPTIVE_FWO_EXPLICIT_H
#define P_ADAPTIVE_FWO_EXPLICIT_H

#include <medusa/Medusa.hpp>

template <typename vec_t, typename approx_t>
Eigen::VectorXd fwo_explicit(const mm::XML& conf, mm::DomainDiscretization<vec_t>& domain,
                             mm::VectorField2d& disp_field, mm::Range<approx_t> engines) {
    const bool debug = conf.get<bool>("debug.print");
    if (debug) {
        std::cout << "Computing explicit field ..." << std::endl;
    }

    // Load params.
    double E = conf.get<double>("phy.E");
    double nu = conf.get<double>("phy.nu");
    // Parameter logic.
    double mu = E / 2. / (1 + nu);
    double lam = E * nu / (1 - 2 * nu) / (1 + nu);
    auto state = conf.get<std::string>("phy.state");
    assert_msg((state == "plane stress" || state == "plane strain"),
               "State is neither plane "
               "stress nor plane strain, but got '%s'.",
               state);
    if (state == "plane stress") {
        lam = 2 * mu * lam / (2 * mu + lam);  // plane stress
    }

    // Node types.
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;

    mm::Range<mm::Range<int>> node_types;
    for (int j = min_order; j <= max_order; j += step) {
        // We wish to approximate with increased order.
        mm::Range<int> _range = domain.types().filter([&](int i) { return abs(i) == j; });
        node_types.push_back(_range);
    }

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }

    // Support sizes.
#pragma omp parallel for default(none) shared(node_types, domain, min_order, only_even_orders)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            int support_size_order =
                2 * binomialCoeff(2 * (i + (only_even_orders ? 1 : 0)) + min_order + vec_t::dim,
                                  vec_t::dim);
            mm::FindClosest f(support_size_order);
            f.forNodes(node_types[i]);
            domain.findSupport(f);
        }
    }

    // Shapes.
    if (debug) {
        std::cout << "Computing shapes ..." << std::endl;
    }
    std::tuple<mm::Lap<mm::Vec2d::dim>, mm::Der1s<mm::Vec2d::dim>, mm::Der2s<mm::Vec2d::dim>>
        operators;
    mm::RaggedShapeStorage<vec_t, decltype(operators)> storage;
    storage.resize(domain.supportSizes());
// Support shapes.
#pragma omp parallel for default(none) shared(node_types, domain, storage, operators, engines)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            computeShapes(domain, engines[i], node_types[i], operators, &storage);
        }
    }

    // Construct explicit operators over our storage.
    if (debug) {
        std::cout << "Constructing operators ..." << std::endl;
    }
    auto op = storage.explicitVectorOperators();

    // Apply operators.
    int N = domain.size();
    mm::VectorField2d sol(N);
    sol.setZero();

    if (debug) {
        std::cout << "Solving ..." << std::endl;
    }
    // Interior.
    for (int i : domain.interior()) {
        sol[i] = (lam + mu) * op.graddiv(disp_field, i) + mu * op.lap(disp_field, i);
    }

    return sol.asLinear();
}

#endif  // P_ADAPTIVE_FWO_EXPLICIT_H
