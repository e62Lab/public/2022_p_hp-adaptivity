//
// Created by mitja on 27/07/2022.
//

#ifndef P_ADAPTIVE_FWO_IMPLICIT_H
#define P_ADAPTIVE_FWO_IMPLICIT_H

#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include "domain_helper.h"
#include "../helpers/math_helper.hpp"

template <typename domain_t, typename approx_t>
std::pair<mm::VectorField2d, mm::VectorField3d> fwo_implicit(const mm::XML& conf, mm::HDF& file,
                                                             domain_t& domain,
                                                             mm::Range<approx_t>& engines,
                                                             Eigen::VectorXd& rhs,
                                                             int iteration_id) {
    const bool debug = conf.get<bool>("debug.print");
    if (debug) {
        std::cout << "Solving implicit with mixed orders..." << std::endl;
    }

    // Approximation orders.
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }
    // Split nodes to ranges with the same approximation order.
    mm::Range<mm::Range<int>> node_types;
    for (int j = min_order; j <= max_order; j += step) {
        mm::Range<int> _range = domain.types().filter([&](int i) { return abs(i) == j; });
        node_types.push_back(_range);
    }

    // Compute support sizes according to approximation order.
#pragma omp parallel for default(none) shared(node_types, domain, min_order, step)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            int support_size =
                2 * binomialCoeff(step * i + min_order + mm::Vec2d::dim, mm::Vec2d::dim);
            mm::FindClosest f(support_size);
            f.forNodes(node_types[i]);
            domain.findSupport(f);
        }
    }

    // Compute shapes.
    if (debug) {
        std::cout << "Computing shapes ..." << std::endl;
    }
    std::tuple<mm::Lap<mm::Vec2d::dim>, mm::Der1s<mm::Vec2d::dim>, mm::Der2s<mm::Vec2d::dim>>
        operators;
    mm::RaggedShapeStorage<mm::Vec2d, decltype(operators)> storage;
    storage.resize(domain.supportSizes());
#pragma omp parallel for default(none) shared(node_types, domain, storage, operators, engines)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            computeShapes(domain, engines[i], node_types[i], operators, &storage);
        }
    }

    if (debug) {
        std::cout << "Shapes computed, loading params." << std::endl;
    }
    // Load params.
    double E = conf.get<double>("phy.E");
    double nu = conf.get<double>("phy.nu");
    double F = conf.get<double>("phy.F");
    double Q = conf.get<double>("phy.Q");
    double COF = conf.get<double>("phy.COF");
    double thickness = conf.get<double>("case.thickness");
    double radius = conf.get<double>("case.radius");
    double sigma_axial = conf.get<double>("phy.sigmaAxial");
    // Parameter logic.
    double mu = E / 2. / (1 + nu);
    double lam = E * nu / (1 - 2 * nu) / (1 + nu);
    auto state = conf.get<std::string>("phy.state");
    assert_msg((state == "plane stress" || state == "plane strain"),
               "State is neither plane "
               "stress nor plane strain, but got '%s'.",
               state);
    if (state == "plane stress") {
        lam = 2 * mu * lam / (2 * mu + lam);  // plane stress
    }
    double Estar = E / (2 * (1 - nu * nu));
    double a = 2 * std::sqrt(std::abs(F * radius / (thickness * mm::PI * Estar)));
    double p0 = std::sqrt(std::abs(F * Estar / (thickness * mm::PI * radius)));
    double c = a * std::sqrt(1 - Q / COF / std::abs(F));
    double e = a * sigma_axial / 4 / COF / p0;

    if (debug) {
        std::cout << "Parameters loaded & computed, solving system." << std::endl;
    }
    int N = domain.size();
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(2 * N, 2 * N);
    Eigen::VectorXd _rhs(2 * N);
    _rhs.setZero();
    M.reserve(storage.supportSizesVec());

    // Construct implicit operators over our storage.
    auto op = storage.implicitVectorOperators(M, _rhs);

    // Interior.
    for (int i : domain.interior()) {
        (lam + mu) * op.graddiv(i) + mu* op.lap(i) = 0.0;
    }

    // BC.
    const double width = conf.get<double>("case.width");
    const double height = conf.get<double>("case.height");
    for (int i : domain.boundary()) {
        mm::Vec2d normal = domain.normal(i);
        int id = get_boundary_id(normal);
        double x = domain.pos(i, 0);
        double trac = 0;

        switch (id) {
            case -4:
                // TOP.
                op.traction(i, lam, mu, {0, 1}) = 1.0;
                if (c <= std::abs(x + e) && std::abs(x) <= a) {
                    trac = -COF * p0 * std::sqrt(1 - (x / a) * (x / a));
                } else if (std::abs(x + e) < c) {
                    trac = -COF * p0 *
                           (std::sqrt(1 - (x / a) * (x / a)) -
                            c / a * std::sqrt(1 - (x + e) * (x + e) / c / c));
                }
                _rhs(i) = trac;
                _rhs(i + N) = (std::abs(x) < a) ? -p0 * std::sqrt(1 - x * x / a / a) : 0;
                break;
            case -2:
                // RIGHT.
                op.traction(i, lam, mu, {1, 0}) = 1.0;
                _rhs(i) = sigma_axial;
                _rhs(i + N) = 0;
                break;
            case -3:
                // BOTTOM.
                op.eq(0).c(0).der1(i, 1) = 0.0;
                op.eq(1).c(1).value(i) = 0;
                _rhs(i) = 0;
                M.coeffRef(i + N, i + N) = 1;  // boundary conditions on the boundary
                _rhs(i + N) = 0;
                break;
            default:
                // LEFT.
                op.value(i) = {0.0, 0.0};
                _rhs(i) = _rhs(i + N) = 0;
        }
    }
    Eigen::VectorXd norms = (M.cwiseAbs() * Eigen::VectorXd::Ones(M.cols())).cwiseInverse();
    M = norms.asDiagonal() * M;
    _rhs = _rhs.cwiseProduct(norms);

    if (debug) {
        std::cout << "Compute ..." << std::endl;
    }
    Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>, Eigen::IncompleteLUT<double>>
        solver;
    if (conf.get<bool>("solver.preconditioner")) {
        double droptol = conf.get<double>("solver.droptol");
        int fill_factor = conf.get<int>("solver.fill_factor");
        double errtol = conf.get<double>("solver.errtol");
        int maxiter = conf.get<int>("solver.maxiter");

        solver.preconditioner().setDroptol(droptol);
        solver.preconditioner().setFillfactor(fill_factor);
        solver.setMaxIterations(maxiter);
        solver.setTolerance(errtol);
    }
    solver.compute(M);

    if (debug) {
        std::cout << "Solve ..." << std::endl;
    }
    Eigen::VectorXd sol = solver.solve(_rhs);
    if (solver.error() > 1e-5) {
        mm::print_red(
            mm::format("Solver did not converge well! Solver error = %f.\n", solver.error()));
    }
    file.writeDoubleAttribute(mm::format("solver_error_%.5i", iteration_id), solver.error());
    if (debug) {
        prn(solver.iterations());
        prn(solver.error());
    }

    // Postprocess.
    mm::VectorField2d u = mm::VectorField2d::fromLinear(sol);
    mm::VectorField3d stress(N);
    auto eop = storage.explicitVectorOperators();
    for (int i = 0; i < N; ++i) {
        auto grad = eop.grad(u, i);
        stress(i, 0) = (2 * mu + lam) * grad(0, 0) + lam * grad(1, 1);
        stress(i, 1) = lam * grad(0, 0) + (2 * mu + lam) * grad(1, 1);
        stress(i, 2) = mu * (grad(0, 1) + grad(1, 0));
    }

    rhs = _rhs;
    return {u, stress};
}

#endif  // P_ADAPTIVE_FWO_IMPLICIT_H
