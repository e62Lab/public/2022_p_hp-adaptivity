#ifndef P_ADAPTIVE_DOMAIN_HELPER_H
#define P_ADAPTIVE_DOMAIN_HELPER_H

#include <medusa/Medusa.hpp>

using namespace mm;

template <typename function_t>
mm::DomainDiscretization<mm::Vec3d> make_domain(const mm::XML& conf, const function_t& dx) {
    double a = conf.get<double>("case.a");
    double eps = conf.get<double>("case.eps");
    BoxShape<Vec3d> box(-a, -eps);

    auto interior_type = conf.get<int>("num.interior");
    mm::DomainDiscretization<mm::Vec3d> domain =
        box.discretizeBoundaryWithDensity(dx, -interior_type);

    mm::GeneralFill<mm::Vec3d> fill_randomized;
    int seed = conf.get<int>("num.fill_seed");
    if (seed == -1) {
        seed = mm::get_seed();
    }
    fill_randomized.seed(seed);
    fill_randomized(domain, dx, interior_type);

    return domain;
}

#endif  // P_ADAPTIVE_DOMAIN_HELPER_H
