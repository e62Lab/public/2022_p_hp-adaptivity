#ifndef P_ADAPTIVE_FWO_HP_ADAPTIVE_H
#define P_ADAPTIVE_FWO_HP_ADAPTIVE_H

#include <medusa/Medusa.hpp>
#include "boussinesq_implicit.h"
#include "boussinesq_explicit.h"
#include "domain_helper.h"
#include "error_helper.h"
#include "modify_order.h"

using namespace std;

void run_hp_adaptive(const mm::XML& conf, mm::HDF& hdf) {
    // Timer initialization.
    mm::Timer timer;
    timer.addCheckPoint("start");

    // Read booleans from input.
    const bool debug = conf.get<bool>("debug.print");
    const bool save_all = conf.get<bool>("meta.all_data");

    // Build domain.
    if (debug) {
        std::cout << "Building domain ..." << std::endl;
    }
    timer.addCheckPoint("domain");
    double dx = conf.get<double>("num.dx");  // Discretization step.
    mm::DomainDiscretization<mm::Vec3d> domain =
        make_domain(conf, [=](const mm::Vec3d&) { return dx; });
    timer.addCheckPoint("domain_created");

    // Engines.
    mm::Range<mm::RBFFD<mm::Polyharmonic<double>, mm::Vec3d, mm::ScaleToClosest>> engines_implicit;
    mm::Range<mm::RBFFD<mm::Polyharmonic<double>, mm::Vec3d, mm::ScaleToClosest>> engines_explicit;
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;
    for (int i = min_order; i <= max_order + step; i += step) {
        mm::RBFFD<mm::Polyharmonic<double>, mm::Vec3d, mm::ScaleToClosest> engine(
            conf.get<int>("approx.phs_order"), i);
        if (i <= max_order) {
            engines_implicit.push_back(engine);
        }
        if (i > min_order) {
            engines_explicit.push_back(engine);
        }
    }

    // ******************************
    // Iterate hp-adaptive procedure.
    // ******************************
    int max_adaptivity_iterations = conf.get<int>("adaptivity.max_iterations");
    int adaptivity_iterations = 0;
    double min_indicator_reduction = conf.get<double>("adaptivity.stop.imex_reduction");
    double initial_indicator;
    mm::Range<mm::Vec3d> old_sol_implicit(domain.size(), 0);

    do {
        timer.addCheckPoint(mm::format("iteration_%.5i_start", adaptivity_iterations));
        if (debug) {
            cout << "----------------" << endl;
            cout << "  Iteration id: " << adaptivity_iterations << endl;
            prn(domain.size());
        }
        // Save domain to HDF.
        if (save_all) {
            hdf.atomic().writeDomain(mm::format("domain_%.5i", adaptivity_iterations), domain);
        }
        // Save domain size to HDF.
        hdf.atomic().writeIntAttribute(mm::format("N_%.5i", adaptivity_iterations), domain.size());

        // Solve implicit.
        timer.addCheckPoint(mm::format("implicit_%.5i_start", adaptivity_iterations));
        mm::VectorField3d disp_implicit;             // Displacement vector field.
        mm::VectorField<double, 6> stress_implicit;  // Stress vector field.
        Eigen::VectorXd rhs_implicit;                // Implicit RHS.
        Range<double> guess_xx(domain.size(), 0), guess_yy(domain.size(), 0),
            guess_zz(domain.size(), 0);
        if (conf.get<bool>("solver.use_guess")) {
            for (int i = 0; i < domain.size(); ++i) {
                guess_xx[i] = old_sol_implicit[i][0];
                guess_yy[i] = old_sol_implicit[i][1];
                guess_zz[i] = old_sol_implicit[i][2];
            }
        }

        mm::SheppardInterpolant guess_sheppard_x(domain.positions(), guess_xx);
        mm::SheppardInterpolant guess_sheppard_y(domain.positions(), guess_yy);
        mm::SheppardInterpolant guess_sheppard_z(domain.positions(), guess_zz);

        std::tie(disp_implicit, stress_implicit) = boussinesq_implicit(
            conf, hdf, domain, engines_implicit, rhs_implicit, adaptivity_iterations, timer, hdf,
            guess_sheppard_x, guess_sheppard_y, guess_sheppard_z);
        if (conf.get<bool>("solver.use_guess")) {
            old_sol_implicit.resize(disp_implicit.size(), 0);
            for (int i = 0; i < disp_implicit.rows(); ++i) {
                old_sol_implicit[i] = mm::Vec3d{disp_implicit(i)};
            }
        }

        // Save displacements and stresses to file.
        if (save_all) {
            hdf.atomic().writeEigen(mm::format("disp_implicit_%.5i", adaptivity_iterations),
                                    disp_implicit);
            hdf.atomic().writeEigen(mm::format("stress_implicit_%.5i", adaptivity_iterations),
                                    stress_implicit);
        }
        timer.addCheckPoint(mm::format("implicit_%.5i_end", adaptivity_iterations));
        // Compute analytic error.
        double err_inf;
        Eigen::VectorXd err_analytic(domain.size());
        std::tie(err_inf, err_analytic) =
            compute_error(hdf, disp_implicit, domain, conf, adaptivity_iterations);
        if (debug) {
            prn(err_inf);
        }
        // Compute error indicator.
        Eigen::VectorXd indicator_field(domain.size());
        indicator_field.setZero();
        // IMEX indicator.
        timer.addCheckPoint(mm::format("imex_%.5i_start", adaptivity_iterations));
        Eigen::VectorXd disp_explicit =
            boussinesq_explicit(conf, domain, disp_implicit, engines_explicit);
        if (save_all) {
            hdf.atomic().writeEigen(mm::format("disp_explicit_%.5i", adaptivity_iterations),
                                    disp_explicit);
        }
        // Obtain IMEX vector field.
        auto indicator_vector_field = mm::VectorField3d::fromLinear(
            (Eigen::VectorXd)(disp_explicit - rhs_implicit).cwiseAbs());
        // Compute IMEX as linear.
#pragma omp parallel for default(none) shared(indicator_field, indicator_vector_field, domain)
        for (int i = 0; i < domain.size(); ++i) {
            for (int j = 0; j < 3; ++j) {
                // indicator_field[i] += log(1.0 + indicator_vector_field[i][j]);
                indicator_field[i] = indicator_vector_field[i].norm();
            }
        }
        //        indicator_field = err_analytic;

        timer.addCheckPoint(mm::format("imex_%.5i_end", adaptivity_iterations));

        // Write indicator field to file.
        if (save_all) {
            hdf.atomic().writeDoubleArray(mm::format("indicator_%.5i", adaptivity_iterations),
                                          indicator_field);
        }
        // Write max indicator value to file.
        hdf.atomic().writeDoubleAttribute(mm::format("max_indicator_%.5i", adaptivity_iterations),
                                          indicator_field.maxCoeff());

        //        // Recompute stopping criterion.
        //        double max_indicator = indicator_field.maxCoeff();
        //        // Store max initial indicator values.
        //        if (adaptivity_iterations == 0) {
        //            initial_indicator = max_indicator;
        //        }
        //        // Stopping criteria.
        //        double indicator_reduction = max_indicator / initial_indicator;
        //        if (debug) {
        //            prn(indicator_reduction);
        //        }
        //        if (indicator_reduction <= min_indicator_reduction) {
        //            mm::print_green(
        //                mm::format("Convergence criteria reached. Indicator reduced by %f
        //                (required % f)",
        //                           indicator_reduction, min_indicator_reduction));
        //            timer.addCheckPoint(mm::format("iteration_%.5i_end",adaptivity_iterations));
        //            break;
        //        }

        // Apply adaptivity logic.
        timer.addCheckPoint(mm::format("refinement_%.5i_start", adaptivity_iterations));
        apply_adaptivity_logic(conf, domain, hdf, adaptivity_iterations, indicator_field);
        timer.addCheckPoint(mm::format("refinement_%.5i_end", adaptivity_iterations));

        // Next iteration.
        timer.addCheckPoint(mm::format("iteration_%.5i_end", adaptivity_iterations));
        adaptivity_iterations++;

    } while ((adaptivity_iterations < max_adaptivity_iterations));

    hdf.atomic().writeIntAttribute("refinement_iterations", adaptivity_iterations);
    if (adaptivity_iterations == max_adaptivity_iterations) {
        mm::print_red("Stopped by maximum iteration count.\n");
    }

    // Finish.
    timer.addCheckPoint("end");
    hdf.atomic().writeTimer("timer", timer);
    prn(timer.duration("start", "end"));
}

#endif  // P_ADAPTIVE_FWO_HP_ADAPTIVE_H
