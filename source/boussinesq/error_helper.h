#ifndef P_ADAPTIVE_ERROR_HELPER_H
#define P_ADAPTIVE_ERROR_HELPER_H

#include <medusa/Medusa.hpp>
#include "analytic.h"

std::pair<double, Eigen::VectorXd> compute_error(mm::HDF& hdf, mm::VectorField3d& disp_num,
                                                 DomainDiscretization<mm::Vec3d>& domain,
                                                 const mm::XML& conf, int iteration_id) {
    // Physical parameters
    const double E = conf.get<double>("phy.E");
    const double nu = conf.get<double>("phy.nu");
    const double P = -conf.get<double>("phy.P");

    // Derived parameters
    double lam = E * nu / (1 - 2 * nu) / (1 + nu);
    const double mu = E / 2 / (1 + nu);

    // Obtain analytic displacements.
    mm::VectorField3d disp_ana(domain.size());
    for (int i = 0; i < domain.size(); ++i) {
        disp_ana[i] = analytical(domain.pos(i), P, E, nu);
    }

    // Obtain displacement magnitude difference norms.
    Eigen::VectorXd err_disp_num(domain.size());
    Eigen::VectorXd err_disp_ana(domain.size());
    for (int i = 0; i < domain.size(); ++i) {
        err_disp_num[i] = disp_num[i].norm();
        err_disp_ana[i] = disp_ana[i].norm();
    }

    // Error.
    Eigen::VectorXd err = err_disp_ana - err_disp_num;
    hdf.atomic().writeEigen(mm::format("analytic_error_%.5i", iteration_id), err.cwiseAbs());

    // Compute error norm.
    double err_norm_1 = err.lpNorm<1>() / err_disp_ana.lpNorm<1>();
    double err_norm_2 = err.lpNorm<2>() / err_disp_ana.lpNorm<2>();
    double err_norm_inf = err.lpNorm<Eigen::Infinity>() / err_disp_ana.lpNorm<Eigen::Infinity>();

    // Add error norms to hdf.
    hdf.atomic().writeDoubleAttribute(mm::format("err_norm_1_%.5i", iteration_id), err_norm_1);
    hdf.atomic().writeDoubleAttribute(mm::format("err_norm_2_%.5i", iteration_id), err_norm_2);
    hdf.atomic().writeDoubleAttribute(mm::format("err_norm_inf_%.5i", iteration_id), err_norm_inf);

    return {err_norm_inf, err};
}
#endif  // P_ADAPTIVE_ERROR_HELPER_H
