#ifndef P_ADAPTIVE_BOUSSINESQ_IMPLICIT_H
#define P_ADAPTIVE_BOUSSINESQ_IMPLICIT_H

#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include "domain_helper.h"
#include "../helpers/math_helper.hpp"
#include "analytic.h"
// #include <Eigen/PardisoSupport>

//template <typename vec_t>
//Eigen::VectorXd getGuess(const mm::DomainDiscretization<vec_t>& domain, double P, double E,
//                         double nu) {
//    Eigen::VectorXd guess(vec_t::dim * domain.size());
//    guess.setZero();
//
//    for (int i = 0; i < domain.size(); ++i) {
//        vec_t ana = analytical(domain.pos(i), P, E, nu);
//        guess(i) = ana[0];
//        guess(i + domain.size()) = ana[1];
//        guess(i + 2 * domain.size()) = ana[2];
//    }
//
//    return guess;
//}

template <typename vec_t>
Eigen::VectorXd getGuess(const mm::DomainDiscretization<vec_t>& domain, const mm::XML& conf,
                         mm::SheppardInterpolant<vec_t, double>& guess_sheppard_x,
                         mm::SheppardInterpolant<vec_t, double>& guess_sheppard_y,
                         mm::SheppardInterpolant<vec_t, double>& guess_sheppard_z) {
    Eigen::VectorXd guess(vec_t::dim * domain.size());
    guess.setZero();

    for (int i = 0; i < domain.size(); ++i) {
        vec_t p = domain.pos(i);
        guess(i) = guess_sheppard_x(p, conf.get<int>("sheppard.closest_for_guess"));
        guess(i + domain.size()) = guess_sheppard_y(p, conf.get<int>("sheppard.closest_for_guess"));
        guess(i + 2 * domain.size()) =
            guess_sheppard_z(p, conf.get<int>("sheppard.closest_for_guess"));
    }

    return guess;
}

template <typename domain_t, typename approx_t>
std::pair<mm::VectorField3d, VectorField<double, 6>> boussinesq_implicit(
    const mm::XML& conf, mm::HDF& file, domain_t& domain, mm::Range<approx_t>& engines,
    Eigen::VectorXd& rhs, int iteration_id, mm::Timer& timer, mm::HDF& hdf,
    mm::SheppardInterpolant<mm::Vec3d, double>& guess_sheppard_x,
    mm::SheppardInterpolant<mm::Vec3d, double>& guess_sheppard_y,
    mm::SheppardInterpolant<mm::Vec3d, double>& guess_sheppard_z) {
    const bool debug = conf.get<bool>("debug.print");
    if (debug) {
        std::cout << "Solving implicit with mixed orders..." << std::endl;
    }

    // Approximation orders.
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }
    // Split nodes to ranges with the same approximation order.
    mm::Range<mm::Range<int>> node_types;
    for (int j = min_order; j <= max_order; j += step) {
        mm::Range<int> _range = domain.types().filter([&](int i) { return abs(i) == j; });
        node_types.push_back(_range);
    }

    // Compute support sizes according to approximation order.
#pragma omp parallel for default(none) shared(node_types, domain, min_order, step)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            int support_size =
                2 * binomialCoeff(step * i + min_order + mm::Vec3d::dim, mm::Vec3d::dim);
            mm::FindClosest f(support_size);
            f.forNodes(node_types[i]);
            domain.findSupport(f);
        }
    }

    // Compute shapes.
    if (debug) {
        std::cout << "Computing shapes ..." << std::endl;
    }
    std::tuple<mm::Lap<mm::Vec3d::dim>, mm::Der1s<mm::Vec3d::dim>, mm::Der2s<mm::Vec3d::dim>>
        operators;
    mm::RaggedShapeStorage<mm::Vec3d, decltype(operators)> storage;
    storage.resize(domain.supportSizes());
#pragma omp parallel for default(none) shared(node_types, domain, storage, operators, engines)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            computeShapes(domain, engines[i], node_types[i], operators, &storage);
        }
    }

    if (debug) {
        std::cout << "Shapes computed, loading params." << std::endl;
    }
    // Physical parameters
    const double E = conf.get<double>("phy.E");
    const double nu = conf.get<double>("phy.nu");
    const double P = -conf.get<double>("phy.P");

    // Derived parameters
    double lam = E * nu / (1 - 2 * nu) / (1 + nu);
    const double mu = E / 2 / (1 + nu);

    if (debug) {
        std::cout << "Parameters loaded & computed, solving system." << std::endl;
    }
    int N = domain.size();
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(3 * N, 3 * N);
    Eigen::VectorXd _rhs(3 * N);
    _rhs.setZero();
    M.reserve(storage.supportSizesVec());

    // Construct implicit operators over our storage.
    auto op = storage.implicitVectorOperators(M, _rhs);

    // Interior.
    for (int i : domain.interior()) {
        (lam + mu) * op.graddiv(i) + mu* op.lap(i) = 0.0;
    }

    // BC.
    for (int i : domain.boundary()) {
        op.value(i) = analytical(domain.pos(i), P, E, nu);
    }

    // *******
    // SOLVER
    // *******
    string solver_type = conf.get<string>("solver.type");
    if (debug) {
        std::cout << "Solving with: " << solver_type << " ..." << std::endl;
    }
    Eigen::VectorXd u_sol(3 * N);
    if (solver_type == "lu") {
        Eigen::SparseLU<decltype(M)> solver;
        solver.compute(M);
        timer.addCheckPoint(mm::format("solver_start_%.5i", iteration_id));
        Eigen::VectorXd sol = solver.solve(_rhs);
        timer.addCheckPoint(mm::format("solver_stop_%.5i", iteration_id));

        if (solver.info() == Eigen::Success) {
            if (debug) {
                mm::print_green("Solver converged!\n");
            }
        } else {
            mm::print_red("Solver did not converge!\n");
            Eigen::VectorXd sol = Eigen::VectorXd::Zero(domain.size());
        }
        u_sol = sol;
        hdf.atomic().writeIntAttribute(mm::format("N_solver_iters_%.5i", iteration_id), 1);
        if (conf.get<bool>("meta.all_data")) {
            hdf.atomic().writeSparseMatrix(mm::format("Matrix_%.5i", iteration_id), M);
        }
    } else if (solver_type == "qr") {
        Eigen::SparseQR<decltype(M), Eigen::COLAMDOrdering<int>> solver;
        M.makeCompressed();
        solver.compute(M);
        timer.addCheckPoint(mm::format("solver_start_%.5i", iteration_id));
        Eigen::VectorXd sol = solver.solve(_rhs);
        timer.addCheckPoint(mm::format("solver_stop_%.5i", iteration_id));

        if (solver.info() == Eigen::Success) {
            if (debug) {
                mm::print_green("Solver converged!\n");
            }
        } else {
            mm::print_red("Solver did not converge!\n");
            Eigen::VectorXd sol = Eigen::VectorXd::Zero(domain.size());
        }
        u_sol = sol;
        hdf.atomic().writeIntAttribute(mm::format("N_solver_iters_%.5i", iteration_id), 1);
        if (conf.get<bool>("meta.all_data")) {
            hdf.atomic().writeSparseMatrix(mm::format("Matrix_%.5i", iteration_id), M);
        }
    } else if (solver_type == "bicgstab") {
        Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;

        if (conf.get<bool>("solver.preconditioner.use")) {
            solver.preconditioner().setFillfactor(conf.get<int>("solver.preconditioner.fill"));
            solver.preconditioner().setDroptol(conf.get<double>("solver.preconditioner.drop_tol"));
            solver.setMaxIterations(conf.get<int>("solver.preconditioner.max_iter"));
            solver.setTolerance(conf.get<double>("solver.preconditioner.global_tol"));
        }
        solver.compute(M);
        // Compute good guess to reduce system solving time.
//                Eigen::VectorXd guess = getGuess(domain, P, E, nu);
        Eigen::VectorXd guess =
            getGuess(domain, conf, guess_sheppard_x, guess_sheppard_y, guess_sheppard_z);
        timer.addCheckPoint(mm::format("solver_start_%.5i", iteration_id));
        if (conf.get<bool>("solver.use_guess")) {
            Eigen::VectorXd sol = solver.solveWithGuess(_rhs, guess);
            u_sol = sol;
        } else {
            Eigen::VectorXd sol = solver.solve(_rhs);
            u_sol = sol;
        }
        timer.addCheckPoint(mm::format("solver_stop_%.5i", iteration_id));
        hdf.atomic().writeIntAttribute(mm::format("N_solver_iters_%.5i", iteration_id),
                                       solver.iterations());
        if (conf.get<bool>("meta.all_data")) {
            hdf.atomic().writeSparseMatrix(mm::format("Matrix_%.5i", iteration_id), M);
        }
        if (debug) {
            prn(solver.iterations());
            prn(solver.error());
        }
    } else if (solver_type == "pu") {
        //                Eigen::PardisoLU<decltype(M)> solver;
        //                Eigen::SparseMatrix<double, Eigen::RowMajor> M2(M);
        //                M2.makeCompressed();
        //                solver.compute(M2);
        //                timer.addCheckPoint(mm::format("solver_start_%.5i", iteration_id));
        //                Eigen::VectorXd solution = solver.solve(_rhs);
        //                timer.addCheckPoint(mm::format("solver_stop_%.5i", iteration_id));
        //                u_sol = solution;
        //                if (conf.get<bool>("meta.all_data")) {
        //                    hdf.atomic().writeSparseMatrix(mm::format("Matrix_%.5i",
        //                    iteration_id), M);
        //                }
        //                hdf.atomic().writeIntAttribute(mm::format("N_solver_iters_%.5i",
        //                iteration_id), 1);
    } else {
        assert_msg(false, "Unknown solver type: '%s'", solver_type);
    }
    hdf.atomic().writeDoubleAttribute(mm::format("solver_duration_%.5i", iteration_id),
                                      timer.duration(mm::format("solver_start_%.5i", iteration_id),
                                                     mm::format("solver_stop_%.5i", iteration_id)));
    // Postprocess.
    mm::VectorField3d u = mm::VectorField3d::fromLinear(u_sol);
    auto eop = storage.explicitVectorOperators();
    VectorField<double, 6> stress(N);
    for (int i = 0; i < N; ++i) {
        Eigen::Matrix3d grad = eop.grad(u, i);
        Eigen::Matrix3d eps = 0.5 * (grad + grad.transpose());
        Eigen::Matrix3d s = lam * eps.trace() * Eigen::Matrix3d::Identity(3, 3) + 2 * mu * eps;
        stress[i][0] = s(0, 0);
        stress[i][1] = s(1, 1);
        stress[i][2] = s(2, 2);
        stress[i][3] = s(0, 1);
        stress[i][4] = s(0, 2);
        stress[i][5] = s(1, 2);
    }

    rhs = _rhs;
    return {u, stress};
}

#endif  // P_ADAPTIVE_BOUSSINESQ_IMPLICIT_H
