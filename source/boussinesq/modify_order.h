//
// Created by mitja on 03/08/2022.
//

#ifndef P_ADAPTIVE_MODIFY_ORDER_H
#define P_ADAPTIVE_MODIFY_ORDER_H

#include <math.h>
#include <medusa/Medusa.hpp>
#include "../helpers/math_helper.hpp"
#include "domain_helper.h"

using namespace std;

template <typename vec_t>
std::pair<int, int> apply_p_adaptivity_logic(const mm::XML& conf,
                                             mm::DomainDiscretization<vec_t>& domain,
                                             const Eigen::VectorXd& indicator_field) {
    // Params.
    const double max_indicator = indicator_field.maxCoeff();
    const double min_indicator = indicator_field.minCoeff();
    const double delta1 = conf.get<double>("adaptivity.p.delta1");
    const double delta2 = conf.get<double>("adaptivity.p.delta2");
    const auto min_order = conf.get<int>("approx.min_order");
    const auto max_order = conf.get<int>("approx.max_order");
    const double lower_indicator_bound = delta1 * max_indicator;
    const double upper_indicator_bound = delta2 * max_indicator;
    const bool do_derefine = conf.get<bool>("adaptivity.derefine.p.do");
    const double refine_agressiveness = conf.get<double>("adaptivity.p.agressiveness");
    const double derefine_agressiveness = conf.get<double>("adaptivity.derefine.p.agressiveness");

    // Modify augmentation if required.
    int refined = 0;    // Just for stats.
    int derefined = 0;  // Just for stats.
    for (int i : domain.all()) {
        int current_type = domain.type(i);  // Current type.
        int new_type = current_type;        // New type placeholder.

        // Compute new order.
        if (indicator_field[i] >= upper_indicator_bound) {
            // Increase order.
            double badness = (indicator_field[i] - upper_indicator_bound) /
                             (max_indicator - upper_indicator_bound);
            new_type = (int)std::round(abs(current_type) /
                                       (badness * (1.0 / refine_agressiveness - 1.0) + 1.0));
            new_type = min(new_type, max_order);  // Limit.
            new_type *= sgn(current_type);        // Sign.

            // Stats.
            if (new_type != current_type) {
                refined++;
            }
        } else if (indicator_field[i] <= lower_indicator_bound) {
            // Decrease order.
            if (do_derefine) {
                double badness = (lower_indicator_bound - indicator_field[i]) /
                                 (lower_indicator_bound - min_indicator);
                new_type = (int)std::round(abs(current_type) /
                                           (badness * (derefine_agressiveness - 1.0) + 1.0));
                new_type = max(new_type, min_order);  // Limit.
                new_type *= sgn(current_type);        // Sign.

                // Stats.
                if (new_type != current_type) {
                    derefined++;
                }
            }
        }

        // Store new type.
        domain.type(i) = new_type;
    }

    return {refined, derefined};
}

template <typename vec_t>
std::pair<int, int> apply_h_adaptivity_logic(const mm::XML& conf,
                                             mm::DomainDiscretization<vec_t>& domain,
                                             const Eigen::VectorXd& indicator_field) {
    // Params.
    const double max_indicator = indicator_field.maxCoeff();
    const double min_indicator = indicator_field.minCoeff();
    const double delta1 = conf.get<double>("adaptivity.h.delta1");
    const double delta2 = conf.get<double>("adaptivity.h.delta2");
    const double lower_indicator_bound = delta1 * max_indicator;
    const double upper_indicator_bound = delta2 * max_indicator;
    const bool do_derefine = conf.get<bool>("adaptivity.derefine.h.do");
    const double agressiveness_deref = conf.get<double>("adaptivity.derefine.h.agressiveness");
    const double agressiveness_ref = conf.get<double>("adaptivity.h.agressiveness");
    const double max_dx = conf.get<double>("adaptivity.h.max_dx");

    // Modify augmentation if required.
    int refined = 0;       // Just for stats.
    int derefined = 0;     // Just for stats.
    auto d_temp = domain;  // Domain copy.
    int N = d_temp.size();
    mm::Range<double> new_dx(N);  // New nodal distances.
    bool max_domain_size_breached = domain.size() > conf.get<double>("adaptivity.h.max_nodes");
    for (int i = 0; i < N; ++i) {
        double old_dx = d_temp.dr(i);
        if ((indicator_field(i) >= upper_indicator_bound) && !max_domain_size_breached) {
            double badness = (indicator_field(i) - upper_indicator_bound) /
                             (max_indicator - upper_indicator_bound);
            new_dx[i] = old_dx / (badness * (agressiveness_ref - 1) + 1);
            if (abs(1 - new_dx[i] / old_dx) > 0.05) {
                refined++;
            }
        } else if (indicator_field(i) <= lower_indicator_bound) {
            double badness = (lower_indicator_bound - indicator_field(i)) /
                             (lower_indicator_bound - min_indicator);
            new_dx[i] = old_dx / (1 + badness * (1.0 / agressiveness_deref - 1));
            if (new_dx[i] < old_dx) {
                exit(1);
            }
            if (new_dx[i] > max_dx) {
                // Proposed new dx was too large, so reduce it to ll.
                new_dx[i] = max_dx;
            } else {
                if (abs(1 - new_dx[i] / old_dx) > 0.05) {
                    derefined++;
                }
            }
        } else {
            new_dx[i] = old_dx;
        }
    }

    // Rebuild domain.
    mm::SheppardInterpolant<vec_t, double> interpolant_dx(domain.positions(), new_dx);
    mm::Range<double> types_double(N);
    for (int i = 0; i < N; ++i) {
        types_double[i] = (double)abs(domain.type(i));
    }
    mm::SheppardInterpolant<vec_t, double> interpolant_types(domain.positions(), types_double);

    // Domain discretization.
    int closest = conf.get<int>("sheppard.closest_for_dx");
    auto h = [&](const vec_t& p) { return interpolant_dx(p, closest); };
    mm::DomainDiscretization<vec_t> d = make_domain(conf, h);

    // Relax.
    if (conf.get<bool>("relax.do")) {
        cout << "Before relax.. " << d.size() << endl;
        mm::BasicRelax relax;
        const int iter = conf.get<int>("relax.iter");
        const int neighbours = conf.get<int>("relax.neighbours");
        const double initial_heat = conf.get<double>("relax.initial_heat");
        const double final_heat = conf.get<double>("relax.final_heat");
        relax.iterations(iter)
            .projectionType(mm::BasicRelax::DO_NOT_PROJECT)
            .numNeighbours(neighbours)
            .initialHeat(initial_heat)
            .finalHeat(final_heat);

        prn("Relaxing ...");
        relax(d, h);

        cout << "After relax .. " << d.size() << endl;
    }

    // Get types for refined domain.
    closest = conf.get<int>("sheppard.closest_for_type");
    const int min_order = conf.get<int>("approx.min_order");
    const int max_order = conf.get<int>("approx.max_order");
    for (int i : d.all()) {
        int sign = (d.type(i) < 0) ? -1 : 1;

        int interpolated_type = (int)std::round(interpolant_types(d.pos(i), closest));
        if (conf.get<bool>("approx.only_even")) {
            interpolated_type = roundUp(interpolant_types(d.pos(i), closest), 2);
        }
        if (interpolated_type < min_order) {
            d.type(i) = sign * min_order;
        } else if (interpolated_type > max_order) {
            d.type(i) = sign * max_order;
        } else {
            d.type(i) = sign * interpolated_type;
        }
    }

    domain = d;

    return {refined, derefined};
}

template <typename vec_t>
void apply_adaptivity_logic(const mm::XML& conf, mm::DomainDiscretization<vec_t>& domain,
                            mm::HDF& hdf, const int iteration,
                            const Eigen::VectorXd& imex_indicator) {
    bool debug = conf.get<bool>("debug.print");
    if (debug) {
        cout << "Adaptivity procedure started ..." << endl;
    }
    // p-refinement.
    int refined = 0, derefined = 0;
    if (conf.get<bool>("adaptivity.p.do")) {
        if (debug) {
            cout << "Order refinement procedure started ..." << endl;
        }
        std::tie(refined, derefined) = apply_p_adaptivity_logic(conf, domain, imex_indicator);
        if (debug) {
            cout << "p-refined: " << refined << ", derefined: " << derefined << endl;
        }
    }
    hdf.atomic().writeIntAttribute(mm::format("order_refined_%.5i", iteration), refined);
    hdf.atomic().writeIntAttribute(mm::format("order_derefined_%.5i", iteration), derefined);

    // h-refinement.
    refined = 0, derefined = 0;
    if (conf.get<bool>("adaptivity.h.do")) {
        if (debug) {
            cout << "Field description refinement procedure started ..." << endl;
        }
        std::tie(refined, derefined) = apply_h_adaptivity_logic(conf, domain, imex_indicator);
        if (debug) {
            cout << "h-refined: " << refined << ", derefined: " << derefined << endl;
        }
    }
    hdf.atomic().writeIntAttribute(mm::format("dx_refined_%.5i", iteration), refined);
    hdf.atomic().writeIntAttribute(mm::format("dx_derefined_%.5i", iteration), derefined);
}

#endif  // P_ADAPTIVE_MODIFY_ORDER_H
