//
// Created by mitja on 02/08/2022.
//

#ifndef P_ADAPTIVE_ANALYTIC_H
#define P_ADAPTIVE_ANALYTIC_H

#include <medusa/Medusa.hpp>

using namespace mm;

Vec3d analytical(const Vec3d& p, double P, double E, double nu) {
    double x = p[0], y = p[1], z = p[2];
    double r = std::sqrt(x * x + y * y);
    double c = x / r, s = y / r;
    double R = p.norm();
    double mu = E / 2 / (1 + nu);
    double u = P * r / 4 / mm::PI / mu * (z / R / R / R - (1 - 2 * nu) / (R * (R + z)));
    double w = P / 4 / mm::PI / mu * (z * z / R / R / R + 2 * (1 - nu) / R);

    return Vec3d{u * c, u * s, w};
};

#endif  // P_ADAPTIVE_ANALYTIC_H
