#include <medusa/Medusa.hpp>
#include "strategy/hp_strategy.h"

using namespace std;
using namespace mm;

Range<XML> quick_generate_XMLS(XML conf) {
    int dim = conf.get<int>("domain.dim");
    string case_type = conf.get<string>("case.name");
    Range<string> required_precisions =
        split(conf.get<string>("adaptivity.stop.imex_reduction"), ',');
    int runs = conf.get<int>("convergence.runs");

    Range<XML> files;
    int N = 0;
    for (string required_precision : required_precisions) {
        for (int i = 0; i < runs; ++i) {
            // Copy master configuration.
            XML _conf(conf);
            _conf.set("adaptivity.stop.imex_reduction", required_precision, true);
            _conf.set("convergence.runs", 1, true);

            // Filename.
            string output_hdf = conf.get<string>("meta.out_file") + "_dim_" + to_string(dim) + "_" +
                                case_type + "_" + to_string(N);
            _conf.set("meta.out_file", output_hdf, true);

            // Finalize.
            files.push_back(_conf);
            N++;
        }
    }

    return files;
}

int main(int argc, const char* argv[]) {
    // Check for settings hdf.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Set num threads.
#if defined(_OPENMP)
    omp_set_num_threads(conf.get<int>("sys.threads"));
#endif

    // Create H5 hdf to store the parameters.
    int dim = conf.get<int>("domain.dim");
    string case_type = conf.get<string>("case.name");
    string output_hdf = conf.get<string>("meta.out_dir") + conf.get<string>("meta.out_file") +
                        "_dim_" + to_string(dim) + "_" + case_type + ".h5";

    cout << "----------" << endl;
    cout << "Computing, convergence " + case_type + " case, please wait ..." << endl;
    if (case_type == "exponential") {
        Range<XML> config_files = quick_generate_XMLS(conf);
        int files = config_files.size();
#pragma omp parallel for default(none) shared(config_files, files, dim, cout)
        for (int i = 0; i < files; ++i) {
            // Input.
            auto _conf = config_files[i];
            auto required_precision = _conf.get<double>("adaptivity.stop.imex_reduction");

            // Create HDF.
            string output_hdf =
                _conf.get<string>("meta.out_dir") + _conf.get<string>("meta.out_file") + ".h5";
            cout << "Creating results hdf: " << output_hdf << endl;
            HDF hdf(output_hdf, HDF::DESTROY);
            hdf.close();

            // Run.
            cout << ".............." << endl;
            cout << "Running precision " << required_precision << " case run " << i << " out of "
                 << files << " ..." << endl;
            hdf.reopen();
            hdf.openGroup(mm::format("/%i", files));
            hdf.writeIntAttribute("run", i);
            hdf.writeDoubleAttribute("precision", required_precision);
            hdf.writeXML("conf", _conf);

            run_hp_adaptive_procedure<Vec2d>(_conf, hdf);

            hdf.close();
        }
    } else {
        assert_msg(false, "Unexpected case type encountered. Expected: ['exponential'].");
    }

    cout << "Calculations saved to: " << output_hdf << "." << endl;

    return 0;
}
