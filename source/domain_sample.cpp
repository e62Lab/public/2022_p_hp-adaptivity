#include <medusa/Medusa.hpp>

using namespace std;
using namespace mm;

int main(int argc, const char* argv[]) {
    // Check for settings hdf.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 hdf to store the parameters.
    string output_hdf =
        conf.get<string>("meta.out_dir") + conf.get<string>("meta.out_file") + ".h5";
    cout << "Creating results hdf: " << output_hdf << endl;
    HDF hdf(output_hdf, HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    // Settings.
    bool debug = conf.get<int>("debug.print") == 1;

    // Domain definition.
    auto example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        return Vec2d(r * cos(t(0)), r * sin(t(0)));
    };

    auto der_example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        double der_r =
            (-1.5 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * sin(3 * t(0)) * sin(1.5 * t(0)) +
             3 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * cos(3 * t(0)) * cos(1.5 * t(0)) *
                 log(abs(cos(1.5 * t(0))))) /
            cos(1.5 * t(0));

        Eigen::Matrix<double, 2, 1> jm;
        jm.col(0) << der_r * cos(t(0)) - r * sin(t(0)), der_r * sin(t(0)) + r * cos(t(0));

        return jm;
    };

    // Define parametric curve's domain.
    BoxShape<Vec1d> param_bs(Vec<double, 1>{0.0}, Vec<double, 1>{2 * PI});

    UnknownShape<Vec2d> shape;
    DomainDiscretization<Vec2d> domain(shape);

    auto gradient_h = [](Vec2d p) {
        double h_0 = 0.005;
        double h_m = 0.03 - h_0;

        return (0.5 * h_m * (p(0) + p(1) + 3.0) + h_0) / 5.0;
    };
    auto hh = [&](Vec2d p) { return 0.001 + 0.02 * p.norm()/2.; };

    GeneralSurfaceFill<Vec2d, Vec1d> gsf;
    gsf.seed(15);
    domain.fill(gsf, param_bs, example_r, der_example_r, hh);

    GeneralFill<Vec2d> gf;
    gf.seed(15);
    domain.fill(gf, hh);


    auto part_4 = domain.positions().filter([](const Vec2d &v) { return v[0] < 0.1; });
    auto part_6 = domain.positions().filter([](const Vec2d &v) { return (v[0] >= 0.1) && (v[0] < 0.35); });
    auto part_2 = domain.positions().filter([](const Vec2d &v) { return (v[0] >= 0.35) && (v[1] > 1.4); });
    auto part_8 = domain.positions().filter([](const Vec2d &v) { return (v[0] >= 0.35) && (v[1] <= 1.4); });

    int N = domain.size();
    domain.findSupport(FindClosest(12).forNodes(part_2).searchAmong(part_2).forceSelf());
    domain.findSupport(FindClosest(30).forNodes(part_4).searchAmong(part_4).forceSelf());
    domain.findSupport(FindClosest(56).forNodes(part_6).searchAmong(part_6).forceSelf());
    domain.findSupport(FindClosest(90).forNodes(part_8).searchAmong(part_8).forceSelf());

    // Store to hdf.
    hdf.atomic().writeDomain("domain", domain);
    Range<Range<int>> supports = domain.supports();
    int max_support_size = 90;
    Range<Range<int>> equal_sized_supports;
    for (int i = 0; i < supports.size(); ++i) {
        Range<int> temp_support(max_support_size, -1);
        for (int j = 0; j < supports[i].size(); ++j) {
            temp_support[j] = supports[i][j];
        }

        equal_sized_supports.push_back(temp_support);
    }
    hdf.atomic().writeInt2DArray("supports", equal_sized_supports);
    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_hdf << "." << endl;

    return 0;
}
