#include <medusa/Medusa.hpp>
#include "helpers/math_helper.hpp"
#include <Eigen/Sparse>

using namespace std;
using namespace mm;

int main(int argc, const char* argv[]) {
    // Check for settings hdf.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 hdf to store the parameters.
    string output_hdf =
        conf.get<string>("meta.out_dir") + conf.get<string>("meta.out_file") + ".h5";
    cout << "Creating results hdf: " << output_hdf << endl;
    HDF hdf(output_hdf, HDF::DESTROY);
    hdf.writeXML("conf", conf);

    // Read booleans from input.
    const bool debug = conf.get<bool>("debug.print");
    const bool save_all = conf.get<bool>("meta.all_data");

    Range<string> nodes = split(conf.get<string>("num.dx"), ',');
    Range<string> mon_orders = split(conf.get<string>("approx.mon_order"), ',');
    int case_id = 0;
    for (int i = 0; i < nodes.size(); ++i) {
        for (string s_mon_order : mon_orders) {
            int mon_order = std::stoi(s_mon_order);
            double dx = std::stod(nodes[i]);

            // Build domain.
            if (debug) {
                std::cout << "Building domain ..." << std::endl;
            }
            const double width = conf.get<double>("case.width");
            const double height = conf.get<double>("case.height");
            mm::BoxShape<mm::Vec2d> shape({-width / 2, -height / 2}, {width / 2, 0});

            auto interior_type = conf.get<int>("num.interior");
            mm::DomainDiscretization<mm::Vec2d> domain =
                shape.discretizeBoundaryWithDensity([=](const mm::Vec2d&) { return dx; });

            mm::GeneralFill<mm::Vec2d> fill_randomized;
            int seed = conf.get<int>("num.fill_seed");
            if (seed == -1) {
                seed = mm::get_seed();
            }
            fill_randomized.seed(seed);
            fill_randomized(
                domain, [=](const mm::Vec2d&) { return dx; }, interior_type);

            hdf.writeDomain(mm::format("domain_%.5i", case_id), domain);
            prn(domain.size());
            // Engine.
            mm::RBFFD<mm::Polyharmonic<double>, mm::Vec2d, mm::ScaleToClosest> engine(
                conf.get<int>("approx.phs_order"), mon_order);

            // Supports.
            int support_size = 2 * binomialCoeff(mon_order + mm::Vec2d::dim, mm::Vec2d::dim);
            domain.findSupport(FindClosest(support_size));

            // Shapes.
            auto storage = domain.computeShapes(engine);

            // Build matrix.
            int N = domain.size();
            Eigen::SparseMatrix<double, Eigen::RowMajor> M(2 * N, 2 * N);
            Eigen::VectorXd rhs(2 * N);
            rhs.setZero();
            M.reserve(storage.supportSizesVec());

            // Physical parameters.
            double E = conf.get<double>("phy.E");
            double nu = conf.get<double>("phy.nu");
            double sigma_axial = conf.get<double>("phy.sigmaAxial");
            // Parameter logic.
            double mu = E / 2. / (1 + nu);
            double lam = E * nu / (1 - 2 * nu) / (1 + nu);

            // Edge labels.
            int LEFT = -1;
            int RIGHT = -2;
            int BOTTOM = -3;
            int TOP = -4;

            // Construct implicit operators over our storage.
            auto op = storage.implicitVectorOperators(M, rhs);

            // Set the governing equations and the boundary conditions.
            for (int i : domain.interior()) {
                (lam + mu) * op.graddiv(i) + mu* op.lap(i) = 0.0;
            }
            for (int i : domain.types() == RIGHT) {
                op.traction(i, lam, mu, {1, 0}) = {0.0, -sigma_axial};
            }
            for (int i : domain.types() == LEFT) {
                op.value(i) = {0.0, 0.0};
            }
            for (int i : domain.types() == TOP) {
                op.traction(i, lam, mu, {0, 1}) = 0.0;
            }
            for (int i : domain.types() == BOTTOM) {
                op.traction(i, lam, mu, {0, -1}) = 0.0;
            }
            Eigen::VectorXd norms = (M.cwiseAbs() * Eigen::VectorXd::Ones(M.cols())).cwiseInverse();
            M = norms.asDiagonal() * M;
            rhs = rhs.cwiseProduct(norms);

            if (debug) {
                std::cout << "Compute ..." << std::endl;
            }
            Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>,
                            Eigen::IncompleteLUT<double>>
                solver;
            if (conf.get<bool>("solver.preconditioner")) {
                double droptol = conf.get<double>("solver.droptol");
                int fill_factor = conf.get<int>("solver.fill_factor");
                double errtol = conf.get<double>("solver.errtol");
                int maxiter = conf.get<int>("solver.maxiter");

                solver.preconditioner().setDroptol(droptol);
                solver.preconditioner().setFillfactor(fill_factor);
                solver.setMaxIterations(maxiter);
                solver.setTolerance(errtol);
            }
            solver.compute(M);

            if (debug) {
                std::cout << "Solve ..." << std::endl;
            }
            Eigen::VectorXd sol = solver.solve(rhs);
            if (solver.error() > 1e-5) {
                mm::print_red(mm::format("Solver did not converge well! Solver error = %f.\n",
                                         solver.error()));
            }
            hdf.writeDoubleAttribute(mm::format("solver_error_%.5i", case_id), solver.error());
            if (debug) {
                prn(solver.iterations());
                prn(solver.error());
            }

            // Postprocess.
            mm::VectorField2d u = mm::VectorField2d::fromLinear(sol);
            mm::VectorField3d stress(N);
            auto eop = storage.explicitVectorOperators();
            for (int i = 0; i < N; ++i) {
                auto grad = eop.grad(u, i);
                stress(i, 0) = (2 * mu + lam) * grad(0, 0) + lam * grad(1, 1);
                stress(i, 1) = lam * grad(0, 0) + (2 * mu + lam) * grad(1, 1);
                stress(i, 2) = mu * (grad(0, 1) + grad(1, 0));
            }

            // Save displacements and stresses to file.
            if (save_all) {
                hdf.writeEigen(mm::format("disp_implicit_%.5i", case_id), u);
                hdf.writeEigen(mm::format("stress_implicit_%.5i", case_id), stress);
            }

            stringstream config;
            config << "case=" << case_id << ",nps=" << nodes[i] << ",m=" << s_mon_order;

            hdf.writeStringAttribute(mm::format("config_%.5i", case_id), config.str());
            case_id++;
        }
    }

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_hdf << "." << endl;

    return 0;
}
