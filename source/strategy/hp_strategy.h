#ifndef RUN_P_ADAPTIVE_PROCEDURE_HPP
#define RUN_P_ADAPTIVE_PROCEDURE_HPP

#include <medusa/Medusa.hpp>
#include "../poisson/solve_implicit.hpp"
#include "../poisson/modify_order.h"
#include "../poisson/domain_helper.h"

using namespace std;

/**
 * Run the solution procedure.
 * @tparam vec_t Template parameter - vector.
 * @param conf Configuration XML object.
 * @param hdf HDF output object.
 * @param timer Timer.
 */
template <typename vec_t>
void run_hp_adaptive_procedure(const mm::XML& conf, mm::HDF& hdf) {
    // Timer initialization.
    mm::Timer timer;
    timer.addCheckPoint("start");

    // Build domain.
    timer.addCheckPoint("domain");
    const bool debug = conf.get<bool>("debug.print");
    const bool save_all = conf.get<bool>("meta.all_data");
    if (debug) {
        std::cout << "Building domain ..." << std::endl;
    }

    const double dx = conf.get<double>("domain.dx");
    auto h = [=](const vec_t& p) { return dx; };
    auto domain = make_domain<vec_t>(conf, h);
    timer.addCheckPoint("domain_created");

    // Engines.
    mm::Range<mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest>> engines_implicit;
    mm::Range<mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest>> engines_explicit;
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;
    for (int i = min_order; i <= max_order + step; i += step) {
        mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest> engine(
            conf.get<int>("approx.phs_order"), i);
        if (i <= max_order) {
            engines_implicit.push_back(engine);
        }
        if (i > min_order) {
            engines_explicit.push_back(engine);
        }
    }

    // *****************************
    // Iterate p-adaptive procedure.
    // *****************************
    int max_adaptivity_iterations = conf.get<int>("adaptivity.max_iterations");
    int adaptivity_iterations = 0;
    double min_indicator_reduction = conf.get<double>("adaptivity.stop.imex_reduction");
    double initital_indicator;

    do {
        timer.addCheckPoint(mm::format("iteration_%.5i_start", adaptivity_iterations));
        if (debug) {
            cout << "----------------" << endl;
            cout << "  Iteration id: " << adaptivity_iterations << endl;
            prn(domain.size());
        }
        // Save domain to HDF.
        if (save_all) {
            hdf.writeDomain(mm::format("domain_%.5i", adaptivity_iterations), domain);
        }
        hdf.writeIntAttribute(mm::format("N_%.5i", adaptivity_iterations), domain.size());

        // Solve implicit.
        timer.addCheckPoint(mm::format("implicit_%.5i_start", adaptivity_iterations));
        Eigen::VectorXd rhs_implicit, sol_implicit;
        std::tie(sol_implicit, rhs_implicit) =
            solve_implicit_mixed_orders<vec_t>(conf, domain, timer, engines_implicit);
        if (save_all) {
            hdf.writeDoubleArray(mm::format("solution_implicit_%.5i", adaptivity_iterations),
                                 sol_implicit);
        }
        timer.addCheckPoint(mm::format("implicit_%.5i_end", adaptivity_iterations));

        // Compute analytic error.
        Eigen::VectorXd sol_analytic(domain.size());
#pragma omp parallel for default(none) shared(sol_analytic, domain, conf)
        for (int i = 0; i < domain.size(); ++i) {
            sol_analytic[i] = u_analytic(domain.pos(i), conf);
        }

        // Compute error indicator.
        Eigen::VectorXd indicator_field = (sol_analytic - sol_implicit).cwiseAbs();
        if (save_all) {
            hdf.writeDoubleArray(mm::format("indicator_%.5i", adaptivity_iterations),
                                 indicator_field);
        }
        hdf.writeDoubleAttribute(mm::format("max_indicator_%.5i", adaptivity_iterations),
                                 indicator_field.maxCoeff());

        // Recompute stopping criterion.
        double max_indicator = indicator_field.maxCoeff();
        // Store max initial indicator values.
        if (adaptivity_iterations == 0) {
            initital_indicator = max_indicator;
        }
        // Stopping criteria.
        double indicator_reduction = max_indicator / initital_indicator;
        if (debug) {
            prn(indicator_reduction);
        }

        // Nodal distances.
        Eigen::VectorXd nodal_distances(domain.size());
        for (int i = 0; i < domain.size(); ++i) {
            nodal_distances[i] = domain.dr(i);
        }
        hdf.writeEigen(mm::format("nodal_distances_%.5i", adaptivity_iterations), nodal_distances);

        if (indicator_reduction <= min_indicator_reduction) {
            mm::print_green(
                mm::format("Convergence criteria reached. IMEX reduced by %f (required %f).\n",
                           indicator_reduction, min_indicator_reduction));
            timer.addCheckPoint(mm::format("iteration_%.5i_end", adaptivity_iterations));
            break;
        }

        // Apply adaptivity logic.
        Eigen::VectorXd _;
        timer.addCheckPoint(mm::format("refinement_%.5i_start", adaptivity_iterations));
        apply_adaptivity_logic(conf, domain, hdf, adaptivity_iterations, indicator_field);
        timer.addCheckPoint(mm::format("refinement_%.5i_end", adaptivity_iterations));

        // Next iteration.
        timer.addCheckPoint(mm::format("iteration_%.5i_end", adaptivity_iterations));
        adaptivity_iterations++;
    } while ((adaptivity_iterations < max_adaptivity_iterations));

    hdf.writeIntAttribute("refinement_iterations", adaptivity_iterations);
    if (adaptivity_iterations == max_adaptivity_iterations) {
        mm::print_red("Stopped by maximum iteration count.\n");
    }

    // End execution.
    timer.addCheckPoint("end");
    hdf.writeTimer("timer", timer);
}

#endif /* RUN_P_ADAPTIVE_PROCEDURE_HPP */