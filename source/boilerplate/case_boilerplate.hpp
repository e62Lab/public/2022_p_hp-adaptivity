#ifndef CASE_BOILERPLATE_HPP
#define CASE_BOILERPLATE_HPP

#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
// #include <Eigen/PardisoSupport>
#include <Eigen/Cholesky>

#include "../helpers/math_helper.hpp"

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif

template <typename vec_t>
struct CaseBoilerplate {
  public:
    /**
     * Computes the stencils and stencil weights.
     * @tparam mask Which shapes will be computed.
     * @param conf XML configuration that g
     * @param args other arguments, see computeStencilsAndWeightsInternal
     * @return storage for computed shapes
     */
    template <mm::sh::shape_flags mask, typename... Args>
    static auto computeStencilsAndWeights(const XML& conf, bool increase_order, Args&&... args) {
        std::string basis = conf.get<string>("approx.basis_type");

        using MatrixType = Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, Eigen::Dynamic>;
        if (basis == "phs") {
            int k = conf.get<int>("approx.phs_order");
            auto mon_degree = conf.get<int>("approx.mon_degree");
            mon_degree = increase_order ? mon_degree + 2 : mon_degree;
            mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest,
                      Eigen::PartialPivLU<MatrixType>>
                engine(k, mon_degree);
            return computeStencilsAndWeightsInternal<mask>(conf, std::forward<Args>(args)...,
                                                           engine, increase_order);
        } else if (basis == "mon") {
            auto mon_degree = conf.get<int>("approx.mon_degree");
            mon_degree = increase_order ? mon_degree + 2 : mon_degree;
            auto sigma_w = conf.get<double>("approx.sigma_w");
            mm::WLS<mm::Monomials<vec_t>, mm::GaussianWeight<vec_t>, mm::ScaleToClosest> engine(
                mm::Monomials<vec_t>(mon_degree), sigma_w);
            return computeStencilsAndWeightsInternal<mask>(conf, std::forward<Args>(args)...,
                                                           engine, increase_order);
        }
        assert_msg(false, "Unknown basis type '%s'.", basis);
        throw std::runtime_error("Unknown basis.");
    }

  private:
    template <mm::sh::shape_flags mask, typename approx_t>
    static auto computeStencilsAndWeightsInternal(const XML& conf,
                                                  mm::DomainDiscretization<vec_t>& domain,
                                                  mm::Timer* timer, approx_t& approx,
                                                  bool increase_order) {
        int n = conf.get<int>("approx.support_size");
        if (n == -1) {
            int mon_degree = conf.get<int>("approx.mon_degree");
            mon_degree = increase_order ? mon_degree + 2 : mon_degree;
            n = 2 * binomialCoeff(mon_degree + vec_t::dim, vec_t::dim);
        }
        if (timer) timer->addCheckPoint("support");
        // Find dir and neu boundaries.
        //        Range<int> dir, neu;
        //        for (int k : domain.boundary()) {
        //            vec_t pos = domain.pos(k);
        //            if (pos[0] > conf.get<double>("domain.neumann_threshold")) {
        //                // Dirichlet.
        //                dir.push_back(k);
        //            } else {
        //                // Neumann.
        //                neu.push_back(k);
        //            }
        //        }
        //        domain.findSupport(mm::FindClosest(n).forNodes(domain.interior() + dir));
        //                // For Neumann boundary.
        //        domain.findSupport(mm::FindClosest(n).forNodes(neu).searchAmong(domain.interior()).forceSelf(true));
        domain.findSupport(mm::FindClosest(n));
        if (timer) timer->addCheckPoint("shapes");
        auto storage = domain.template computeShapes<mask>(approx);
        if (timer) timer->addCheckPoint("matrix");
        return storage;
    }

  public:
    /**
     * Solve and time the solution of Mu = r. The solver
     * @param conf Configuration, where the choice of the solver is read from.
     * @param M Sparse matrix.
     * @param rhs Right hand side.
     * @param timer Timer class.
     * @param hdf If given, the matrix and rhs are saved.
     * @return
     */
    template <typename matrix_t, typename rhs_t>
    static auto sparseSolve(const XML& conf, const matrix_t& M, const rhs_t& rhs,
                            const Eigen::VectorXd& good_guess, mm::Timer* timer = nullptr,
                            mm::HDF* hdf = nullptr) {
        std::string solver_type = conf.get<string>("solver.type");
        using MatrixType = Eigen::SparseMatrix<typename matrix_t::Scalar>;
        // if (solver_type == "pardiso") {
        //     return sparseSolveInternal<Eigen::PardisoLU<MatrixType>>(conf, M, rhs, timer, hdf);
        // } else
        if (solver_type == "bicgstab") {
            return sparseSolveInternal<Eigen::BiCGSTAB<MatrixType, Eigen::IncompleteLUT<double>>>(
                conf, M, rhs, good_guess, timer, hdf);
        } else {
            assert_msg(false, "Unknown solver %s.", solver_type);
            throw std::runtime_error("Unknown solver");
        }
    }

  private:
    template <typename solver_t, typename matrix_t, typename rhs_t>
    static auto sparseSolveInternal(const XML& conf, const matrix_t& M, const rhs_t& rhs,
                                    const Eigen::VectorXd& good_guess, mm::Timer* timer,
                                    mm::HDF* hdf) {
        if (timer) timer->addCheckPoint("compute");
        solver_t solver;
        if (conf.get<bool>("solver.preconditioner.use")) {
            solver.preconditioner().setFillfactor(conf.get<int>("solver.preconditioner.fill"));
            solver.preconditioner().setDroptol(conf.get<double>("solver.preconditioner.drop_tol"));
            solver.setMaxIterations(conf.get<int>("solver.preconditioner.max_iter"));
            solver.setTolerance(conf.get<double>("solver.preconditioner.global_tol"));
        }
        solver.compute(M);
        if (timer) timer->addCheckPoint("solve");
        //        Eigen::Matrix<typename matrix_t::Scalar, Eigen::Dynamic, 1> sol =
        //        solver.solve(rhs);
        Eigen::Matrix<typename matrix_t::Scalar, Eigen::Dynamic, 1> sol =
            solver.solveWithGuess(rhs, good_guess);
        if (timer) timer->addCheckPoint("postprocess");
        return sol;
    }
};

#endif  // CASE_BOILERPLATE_HPP
