#ifndef APPLY_EXPLICIT_OPERATOR_HPP
#define APPLY_EXPLICIT_OPERATOR_HPP

#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include "../helpers/math_helper.hpp"
#include "poisson_helper.hpp"

/**
 * Applies explicit operators to implicitly computed scalar field. Adaptive.
 * @tparam vec_t Vector.
 * @tparam approx_t Approximation engine.
 * @param conf Configuration file.
 * @param domain Domain.
 * @param timer Timer.
 * @param field Implicitly obtained field.
 * @param engines Range of approximation engines.
 * @return Explicit operator field.
 */
template <typename vec_t, typename approx_t>
Eigen::VectorXd apply_explicit_operators_mixed_orders(const mm::XML& conf,
                                                      mm::DomainDiscretization<vec_t>& domain,
                                                      mm::Timer& timer, Eigen::VectorXd& field,
                                                      mm::Range<approx_t> engines) {
    const bool debug = conf.get<bool>("debug.print");
    if (debug) {
        std::cout << "Computing explicit field ..." << std::endl;
    }

    // Node types.
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;

    mm::Range<mm::Range<int>> node_types;
    for (int j = min_order; j <= max_order; j += step) {
        // We wish to approximate with increased order.
        mm::Range<int> _range = domain.types().filter([&](int i) { return abs(i) == j; });
        node_types.push_back(_range);
    }

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }

    // Support sizes.
#pragma omp parallel for default(none) shared(node_types, domain, min_order, only_even_orders)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            int support_size_order =
                2 * binomialCoeff(2 * (i + (only_even_orders ? 1 : 0)) + min_order + vec_t::dim,
                                  vec_t::dim);
            mm::FindClosest f(support_size_order);
            f.forNodes(node_types[i]);
            domain.findSupport(f);
        }
    }

    // Shapes.
    if (debug) {
        std::cout << "Computing shapes ..." << std::endl;
    }
    std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>> operators;
    mm::RaggedShapeStorage<vec_t, decltype(operators)> storage;
    storage.resize(domain.supportSizes());
    // Support shapes.
#pragma omp parallel for default(none) shared(node_types, domain, storage, operators, engines)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            computeShapes(domain, engines[i], node_types[i], operators, &storage);
        }
    }

    // Construct explicit operators over our storage.
    if (debug) {
        std::cout << "Constructing operators ..." << std::endl;
    }
    auto op = storage.explicitOperators();

    int N = field.size();
    Eigen::VectorXd sol(N);
    sol.setZero();

    if (debug) {
        std::cout << "Solving ..." << std::endl;
    }
    // Interior.
    for (int i : domain.interior()) {
        sol[i] = op.lap(field, i);
    }
    // BC.
    for (int i : domain.boundary()) {
        vec_t pos = domain.pos(i);

        if (pos[0] <= conf.get<double>("domain.types.neumann_threshold")) {
            // Neumann BC.
            vec_t normal = domain.normal(i);
            vec_t grad = op.grad(field, i);

            sol[i] = op.neumann(field, i, normal, normal.dot(grad));
        } else {
            // Dirichlet BC.
            sol[i] = field[i];
        }
    }

    return sol;
}

#endif /* APPLY_EXPLICIT_OPERATOR_HPP */
