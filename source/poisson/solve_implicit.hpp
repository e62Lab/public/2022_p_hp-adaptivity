#ifndef SOLVE_IMPLICIT_HPP
#define SOLVE_IMPLICIT_HPP

#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include "../helpers/math_helper.hpp"
#include <Eigen/IterativeLinearSolvers>
#include "poisson_helper.hpp"
// #include <Eigen/PardisoSupport>

using namespace std;

template <typename vec_t>
Eigen::VectorXd getGuess(const mm::DomainDiscretization<vec_t>& domain,
                         mm::SheppardInterpolant<vec_t, double>& sheppard, const mm::XML& conf) {
    Eigen::VectorXd guess(domain.size());
    guess.setZero();

    for (int i = 0; i < domain.size(); ++i) {
        //        guess(i) = u_analytic(domain.pos(i), conf); // for DEBUG
        guess(i) = sheppard(domain.pos(i), conf.get<int>("sheppard.closest_for_guess"));
    }

    return guess;
}

/**
 * Solve problem implicitly.
 * @tparam vec_t Vector type.
 * @tparam approx_t Approximation type.
 * @param conf Configuration file.
 * @param domain Domain.
 * @param timer Timer.
 * @param engines Range of engines.
 * @param old_solution BICGSTAB guess.
 * @return Pair of solution and rhs.
 */
template <typename vec_t, typename approx_t>
std::pair<Eigen::VectorXd, Eigen::VectorXd> solve_implicit_mixed_orders(
    const mm::XML& conf, mm::DomainDiscretization<vec_t>& domain, mm::Timer& timer,
    mm::Range<approx_t>& engines, mm::HDF& hdf, const int iteration,
    mm::SheppardInterpolant<vec_t, double>& guess_sheppard) {
    const bool debug = conf.get<bool>("debug.print");
    if (debug) {
        std::cout << "Solving implicit with mixed orders..." << std::endl;
    }
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }

    // Split nodes to ranges with the same approximation order.
    mm::Range<mm::Range<int>> node_types;
    for (int j = min_order; j <= max_order; j += step) {
        mm::Range<int> _range = domain.types().filter([&](int i) { return abs(i) == j; });
        node_types.push_back(_range);
    }

    // Compute support sizes according to approximation order.
#pragma omp parallel for default(none) shared(node_types, domain, min_order, step)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            int support_size = 2 * binomialCoeff(step * i + min_order + vec_t::dim, vec_t::dim);
            mm::FindClosest f(support_size);
            f.forNodes(node_types[i]);
            domain.findSupport(f);
        }
    }

    // Compute shapes.
    if (debug) {
        std::cout << "Computing shapes ..." << std::endl;
    }
    std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>> operators;  // Laplace and derivative.
    mm::RaggedShapeStorage<vec_t, decltype(operators)> storage;
    storage.resize(domain.supportSizes());
#pragma omp parallel for default(none) shared(node_types, domain, storage, operators, engines)
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            computeShapes(domain, engines[i], node_types[i], operators, &storage);
        }
    }

    int N = domain.size();
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Eigen::VectorXd rhs(N);
    rhs.setZero();
    M.reserve(storage.supportSizes());

    // Construct implicit operators over our storage.
    if (debug) {
        std::cout << "Constructing operators ..." << std::endl;
    }
    auto op = storage.implicitOperators(M, rhs);

    // Interior.
    for (int i : domain.interior()) {
        op.lap(i) = u_laplacian(domain.pos(i), conf);
    }
    // BC.
    for (int i : domain.boundary()) {
        vec_t pos = domain.pos(i);

        if (pos[0] <= conf.get<double>("domain.types.neumann_threshold")) {
            // Neumann BC.
            vec_t normal = domain.normal(i);
            vec_t grad = u_gradient(pos, conf);

            op.neumann(i, normal) = normal.dot(grad);
        } else {
            // Dirichlet BC.
            op.value(i) = u_analytic(domain.pos(i), conf);
        }
    }

    // *******
    // SOLVER
    // *******
    string solver_type = conf.get<string>("solver.type");
    if (debug) {
        std::cout << "Solving with: " << solver_type << " ..." << std::endl;
    }
    Eigen::VectorXd u(N);
    if (solver_type == "lu") {
        Eigen::SparseLU<decltype(M)> solver;
        solver.compute(M);
        timer.addCheckPoint(mm::format("solver_start_%.5i", iteration));
        Eigen::VectorXd sol = solver.solve(rhs);
        timer.addCheckPoint(mm::format("solver_stop_%.5i", iteration));

        if (solver.info() == Eigen::Success) {
            if (debug) {
                mm::print_green("Solver converged!\n");
            }
        } else {
            mm::print_red("Solver did not converge!\n");
            Eigen::VectorXd sol = Eigen::VectorXd::Zero(domain.size());
        }
        u = sol;
        hdf.atomic().writeIntAttribute(mm::format("N_solver_iters_%.5i", iteration), 1);
        if (conf.get<bool>("meta.all_data")) {
            hdf.atomic().writeSparseMatrix(mm::format("Matrix_%.5i", iteration), M);
        }
    } else if (solver_type == "qr") {
        Eigen::SparseQR<decltype(M), Eigen::COLAMDOrdering<int>> solver;
        M.makeCompressed();
        solver.compute(M);
        timer.addCheckPoint(mm::format("solver_start_%.5i", iteration));
        Eigen::VectorXd sol = solver.solve(rhs);
        timer.addCheckPoint(mm::format("solver_stop_%.5i", iteration));

        if (solver.info() == Eigen::Success) {
            if (debug) {
                mm::print_green("Solver converged!\n");
            }
        } else {
            mm::print_red("Solver did not converge!\n");
            Eigen::VectorXd sol = Eigen::VectorXd::Zero(domain.size());
        }
        u = sol;
        hdf.atomic().writeIntAttribute(mm::format("N_solver_iters_%.5i", iteration), 1);
        if (conf.get<bool>("meta.all_data")) {
            hdf.atomic().writeSparseMatrix(mm::format("Matrix_%.5i", iteration), M);
        }
    } else if (solver_type == "bicgstab") {
        Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;

        if (conf.get<bool>("solver.preconditioner.use")) {
            solver.preconditioner().setFillfactor(conf.get<int>("solver.preconditioner.fill"));
            solver.preconditioner().setDroptol(conf.get<double>("solver.preconditioner.drop_tol"));
            solver.setMaxIterations(conf.get<int>("solver.preconditioner.max_iter"));
            solver.setTolerance(conf.get<double>("solver.preconditioner.global_tol"));
        }
        solver.compute(M);
        // Compute good guess to reduce system solving time.
        Eigen::VectorXd guess = getGuess(domain, guess_sheppard, conf);
        timer.addCheckPoint(mm::format("solver_start_%.5i", iteration));
        if (conf.get<bool>("solver.use_guess")) {
            Eigen::VectorXd sol = solver.solveWithGuess(rhs, guess);
            u = sol;
        } else {
            Eigen::VectorXd sol = solver.solve(rhs);
            u = sol;
        }
        timer.addCheckPoint(mm::format("solver_stop_%.5i", iteration));
        hdf.atomic().writeIntAttribute(mm::format("N_solver_iters_%.5i", iteration),
                                       solver.iterations());
        if (conf.get<bool>("meta.all_data")) {
            hdf.atomic().writeSparseMatrix(mm::format("Matrix_%.5i", iteration), M);
        }
        if (debug) {
            prn(solver.iterations());
            prn(solver.error());
        }
    } else if (solver_type == "pu") {
        //        Eigen::PardisoLU<decltype(M)> solver;
        //        Eigen::SparseMatrix<double, Eigen::RowMajor> M2(M);
        //        M2.makeCompressed();
        //        solver.compute(M2);
        //        timer.addCheckPoint(mm::format("solver_start_%.5i", iteration));
        //        Eigen::VectorXd solution = solver.solve(rhs);
        //        timer.addCheckPoint(mm::format("solver_stop_%.5i", iteration));
        //        u = solution;
        //        if (conf.get<bool>("meta.all_data")) {
        //            hdf.atomic().writeSparseMatrix(mm::format("Matrix_%.5i", iteration), M);
        //        }
        //        hdf.atomic().writeIntAttribute(mm::format("N_solver_iters_%.5i", iteration), 1);
    } else {
        assert_msg(false, "Unknown solver type: '%s'", solver_type);
    }

    // Return u.
    return {u, rhs};
}

#endif /* SOLVE_IMPLICIT_HPP */
