#ifndef P_ADAPTIVE_DOMAIN_HELPERS_H
#define P_ADAPTIVE_DOMAIN_HELPERS_H

#include <medusa/Medusa.hpp>

template <typename vec_t, typename function_t>
mm::DomainDiscretization<vec_t> make_domain(const mm::XML& conf, const function_t& f) {
    // Domain shape.
    auto origin = conf.get<double>("domain.origin");
    auto radius = conf.get<double>("domain.radius");
    mm::BallShape<vec_t> shape(origin, radius);

    // Domain discretization.
    auto interior_type = conf.get<int>("domain.types.interior");
    // Fill domain with nodes.
    mm::DomainDiscretization<vec_t> domain = shape.discretizeBoundaryWithDensity(f, -interior_type);
    mm::GeneralFill<vec_t> fill_randomized;
    int seed = conf.get<int>("fill.seed");
    if (seed == -1) {
        seed = mm::get_seed();
    }
    fill_randomized.seed(seed);
    domain.addInternalNode(0.6, interior_type);
    fill_randomized(domain, f, interior_type);

    return domain;
}

#endif  // P_ADAPTIVE_DOMAIN_HELPERS_H
