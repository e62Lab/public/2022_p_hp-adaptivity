#ifndef RUN_P_ADAPTIVE_PROCEDURE_HPP
#define RUN_P_ADAPTIVE_PROCEDURE_HPP

#include <medusa/Medusa.hpp>
#include <functional>
#include "solve_implicit.hpp"
#include "apply_explicit_operators.hpp"
#include "../helpers/compute_error_helper.hpp"
#include "modify_order.h"
#include "domain_helper.h"

using namespace std;
/**
 * Run the solution procedure.
 * @tparam vec_t Template parameter - vector.
 * @param conf Configuration XML object.
 * @param hdf HDF output object.
 * @param timer Timer.
 */
template <typename vec_t>
void run_hp_adaptive_procedure(const mm::XML& conf, mm::HDF& hdf) {
    // Timer initialization.
    mm::Timer timer;
    timer.addCheckPoint("start");

    // Build domain.
    timer.addCheckPoint("domain");
    const bool debug = conf.get<bool>("debug.print");
    const bool save_all = conf.get<bool>("meta.all_data");
    if (debug) {
        std::cout << "Building domain ..." << std::endl;
    }

    const double dx = conf.get<double>("domain.dx");
    auto h = [=](const vec_t& p) { return dx; };
    auto domain = make_domain<vec_t>(conf, h);
    timer.addCheckPoint("domain_created");

    // Engines.
    mm::Range<mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest>> engines_implicit;
    mm::Range<mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest>> engines_explicit;
    int min_order = conf.get<int>("approx.min_order");
    int max_order = conf.get<int>("approx.max_order");
    bool only_even_orders = conf.get<bool>("approx.only_even");
    int step = only_even_orders ? 2 : 1;
    for (int i = min_order; i <= max_order + step; i += step) {
        mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest> engine(
            conf.get<int>("approx.phs_order"), i);
        if (i <= max_order) {
            engines_implicit.push_back(engine);
        }
        if (i > min_order) {
            engines_explicit.push_back(engine);
        }
    }

    // *****************************
    // Iterate p-adaptive procedure.
    // *****************************
    int max_adaptivity_iterations = conf.get<int>("adaptivity.max_iterations");
    int adaptivity_iterations = 0;
    double min_indicator_reduction = conf.get<double>("adaptivity.stop.imex_reduction");
    double initital_indicator;
    mm::Range<double> old_sol_implicit(domain.size());
    for (int i = 0; i < old_sol_implicit.size(); ++i) {
        old_sol_implicit[i] = 0.0;
    }

    do {
        timer.addCheckPoint(mm::format("iteration_%.5i_start", adaptivity_iterations));
        if (debug) {
            cout << "----------------" << endl;
            cout << "  Iteration id: " << adaptivity_iterations << endl;
            prn(domain.size());
        }
        // Save domain to HDF.
        if (save_all) {
            hdf.atomic().writeDomain(mm::format("domain_%.5i", adaptivity_iterations), domain);
        }
        hdf.atomic().writeIntAttribute(mm::format("N_%.5i", adaptivity_iterations), domain.size());

        // Solve implicit.
        timer.addCheckPoint(mm::format("implicit_%.5i_start", adaptivity_iterations));
        Eigen::VectorXd rhs_implicit, sol_implicit;
        mm::SheppardInterpolant guess_sheppard(domain.positions(), old_sol_implicit);

        std::tie(sol_implicit, rhs_implicit) = solve_implicit_mixed_orders<vec_t>(
            conf, domain, timer, engines_implicit, hdf, adaptivity_iterations, guess_sheppard);
        if (conf.get<bool>("solver.use_guess")) {
            old_sol_implicit.resize(sol_implicit.size());
            for (int i = 0; i < sol_implicit.size(); ++i) {
                old_sol_implicit[i] = static_cast<double>(sol_implicit(i));
            }
        }
        if (save_all) {
            hdf.atomic().writeDoubleArray(
                mm::format("solution_implicit_%.5i", adaptivity_iterations), sol_implicit);
        }
        timer.addCheckPoint(mm::format("implicit_%.5i_end", adaptivity_iterations));

        // Compute analytic error.
        Eigen::VectorXd sol_analytic(domain.size());
#pragma omp parallel for default(none) shared(sol_analytic, domain, conf)
        for (int i = 0; i < domain.size(); ++i) {
            sol_analytic[i] = u_analytic(domain.pos(i), conf);
        }
        double inf_error;
        compute_error(sol_implicit, sol_analytic,
                      mm::format("analytic_%.5i", adaptivity_iterations), hdf, inf_error, debug,
                      save_all, true);

        // Compute error indicator.
        Eigen::VectorXd indicator_field;
        // IMEX indicator.
        timer.addCheckPoint(mm::format("imex_%.5i_start", adaptivity_iterations));
        Eigen::VectorXd explicit_operator_field = apply_explicit_operators_mixed_orders(
            conf, domain, timer, sol_implicit, engines_explicit);

        if (save_all) {
            hdf.atomic().writeDoubleArray(
                mm::format("explicit_operator_field_%.5i", adaptivity_iterations),
                explicit_operator_field);
        }
        timer.addCheckPoint(mm::format("imex_%.5i_end", adaptivity_iterations));

        indicator_field = (explicit_operator_field - rhs_implicit).cwiseAbs();
        //        for (int i = 0; i < indicator_field.size(); ++i) {
        //            indicator_field[i] = log(1.0 + indicator_field[i]);
        //        }
        //         STD
        //        auto d_temp = domain;
        //        d_temp.template findSupport(mm::FindClosest(10));
        //        for (int i = 0; i < domain.size(); ++i) {
        //            indicator_field[i] = get_deviation(d_temp.support(i), sol_implicit);
        //        }
        if (save_all) {
            hdf.atomic().writeDoubleArray(mm::format("indicator_imex_%.5i", adaptivity_iterations),
                                          indicator_field);
        }
        hdf.atomic().writeDoubleAttribute(
            mm::format("max_indicator_imex_%.5i", adaptivity_iterations),
            indicator_field.maxCoeff());

        // Recompute stopping criterion.
        double max_indicator = indicator_field.maxCoeff();
        // Store max initial indicator values.
        if (adaptivity_iterations == 0) {
            initital_indicator = max_indicator;
        }
        // Stopping criteria.
        double indicator_reduction = max_indicator / initital_indicator;
        if (debug) {
            prn(indicator_reduction);
        }
        if (indicator_reduction <= min_indicator_reduction) {
            mm::print_green(
                mm::format("Convergence criteria reached. IMEX reduced by %f (required %f).\n",
                           indicator_reduction, min_indicator_reduction));
            timer.addCheckPoint(mm::format("iteration_%.5i_end", adaptivity_iterations));
            break;
        }

        // Apply adaptivity logic.
        Eigen::VectorXd _;
        timer.addCheckPoint(mm::format("refinement_%.5i_start", adaptivity_iterations));
        apply_adaptivity_logic(conf, domain, hdf, adaptivity_iterations, indicator_field);
        timer.addCheckPoint(mm::format("refinement_%.5i_end", adaptivity_iterations));

        // Next iteration.
        timer.addCheckPoint(mm::format("iteration_%.5i_end", adaptivity_iterations));
        adaptivity_iterations++;
    } while ((adaptivity_iterations < max_adaptivity_iterations));

    hdf.atomic().writeIntAttribute("refinement_iterations", adaptivity_iterations);
    if (adaptivity_iterations == max_adaptivity_iterations) {
        mm::print_red("Stopped by maximum iteration count.\n");
    }

    // End execution.
    timer.addCheckPoint("end");
    hdf.atomic().writeTimer("timer", timer);
}

#endif /* RUN_P_ADAPTIVE_PROCEDURE_HPP */
