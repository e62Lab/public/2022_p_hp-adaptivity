#ifndef POISSON_BALL_CUT_HPP
#define POISSON_BALL_CUT_HPP

#include <Eigen/Sparse>
#include "../poisson/poisson_helper.hpp"
#include "../boilerplate/case_boilerplate.hpp"

using namespace mm;
using namespace std;
using namespace Eigen;

template <typename vec_t>
struct PoissonBallCut : public CaseBoilerplate<vec_t> {
    using Base = CaseBoilerplate<vec_t>;

    template <typename fill_t>
    static Eigen::VectorXd solve(const XML& conf, fill_t&& h, DomainDiscretization<vec_t>& domain,
                                 HDF& file, Eigen::VectorXd& rhs) {
        Timer timer;
        // Domain interior and boundary.
        auto interior = domain.interior();
        auto boundary = domain.boundary();

        // Domain size.
        int N = domain.size();

        // Create storage and compute stencils and weights.
        auto storage =
            Base::template computeStencilsAndWeights<sh::lap | sh::d1>(conf, false, domain, &timer);
        SparseMatrix<double, RowMajor> M(N, N);
        rhs = Eigen::VectorXd::Zero(N);

        // Implicit operators.
        auto op = storage.implicitOperators(M, rhs);

        // Interior.
        for (int i : interior) {
            op.lap(i) = u_laplacian(domain.pos(i), conf);
        }

        // BC.
        for (int i : boundary) {
            vec_t pos = domain.pos(i);

            if (pos[0] <= conf.get<double>("domain.neumann_threshold")) {
                // Dirichlet BC.
                op.value(i) = u_analytic(domain.pos(i), conf);
            } else {
                // Neumann BC.
                vec_t normal = domain.normal(i);
                vec_t grad = u_gradient(pos, conf);

                op.neumann(i, normal) = normal.dot(grad);
            }
        }

        if (conf.get<int>("debug.print") == 1) {
            cout << "Problem set, calling solver ..." << endl;
        }

        // Compute good guess to reduce system solving time.
        Eigen::VectorXd good_guess(domain.size());
        for (int i = 0; i < domain.size(); ++i) {
            good_guess(i) = u_analytic(domain.pos(i), conf);
        }

        VectorXd sol = Base::sparseSolve(conf, M, rhs, good_guess, &timer);

        timer.addCheckPoint("solve_end");
        return sol;
    }

    static Eigen::VectorXd apply_explicit_operators(const XML& conf,
                                                    DomainDiscretization<vec_t>& domain, HDF& file,
                                                    const Eigen::VectorXd& u) {
        Timer timer;
        // Domain interior and boundary.
        auto interior = domain.interior();
        auto boundary = domain.boundary();

        // Domain size.
        int N = domain.size();

        // Create storage and compute stencils and weights.
        auto storage =
            Base::template computeStencilsAndWeights<sh::lap | sh::d1>(conf, true, domain, &timer);

        // Obtain explicit operators.
        auto op = storage.explicitOperators();

        Eigen::VectorXd u_op(domain.size());
        // Interior.
        for (int i : domain.interior()) {
            u_op[i] = op.lap(u, i);
        }
        // BC.
        for (int i : domain.boundary()) {
            vec_t pos = domain.pos(i);

            if (pos[0] <= conf.get<double>("domain.neumann_threshold")) {
                // Dirichlet BC.
                u_op[i] = u[i];
            } else {
                // Neumann BC.
                vec_t normal = domain.normal(i);
                vec_t grad = op.grad(u, i);

                u_op[i] = op.neumann(u, i, normal, normal.dot(grad));
            }
        }

        timer.addCheckPoint("explicit_end");
        return u_op;
    }
};

#endif
