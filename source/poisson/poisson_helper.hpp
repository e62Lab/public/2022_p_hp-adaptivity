#ifndef POISSON_HELPER_HPP
#define POISSON_HELPER_HPP

#include <medusa/Medusa.hpp>
#include <math.h>

// STRONG SOURCE
template <typename vec_t>
typename vec_t::scalar_t u_analytic(const vec_t& p, double alpha) {
    vec_t source;
    for (int i = 0; i < vec_t::dim; ++i) {
        source[i] = 1.0 / ((double)i + 2.0);
    }
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    return exp(exponent);
}

template <typename vec_t>
typename vec_t::scalar_t u_laplacian(const vec_t& p, double alpha) {
    vec_t source;
    for (int i = 0; i < vec_t::dim; ++i) {
        source[i] = 1.0 / ((double)i + 2.0);
    }
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    return exp(exponent) * (mm::ipow<2>(alpha * 2.0) * squared_norm - 2.0 * alpha * vec_t::dim);
}

template <typename vec_t>
vec_t u_gradient(const vec_t& p, double alpha) {
    vec_t source;
    for (int i = 0; i < vec_t::dim; ++i) {
        source[i] = 1.0 / ((double)i + 2.0);
    }
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    double constant = -2 * alpha * exp(exponent);

    return constant * (p - source);
}

// LOG SOURCE
template <typename vec_t>
typename vec_t::scalar_t u_analytic(const vec_t& p) {
    return std::log(p.squaredNorm());
}

template <typename vec_t>
typename vec_t::scalar_t u_laplacian(const vec_t& p) {
    return (2 * vec_t::dim - 4) / p.squaredNorm();
}

template <typename vec_t>
vec_t u_gradient(const vec_t& p) {
    return p * 2 / p.squaredNorm();
}

/**
 * Analytic solution
 * @tparam vec_t Vector.
 * @param p Position.
 * @param conf Configuration file
 * @return Analytic solution.
 */
template <typename vec_t>
typename vec_t::scalar_t u_analytic(const vec_t& p, const mm::XML& conf) {
    if (conf.get<string>("case.name") == "exponential") {
        return u_analytic(p, conf.get<double>("case.alpha"));
    } else if (conf.get<string>("case.name") == "log") {
        return u_analytic(p);
    }
    assert_msg(false, "Unknown case name '%s'.", conf.get<string>("case.name"));
    throw std::runtime_error("Unknown case name.");
}

/**
 * Analytic laplacian.
 * @tparam vec_t Vector.
 * @param p Position.
 * @param conf Configuration file.
 * @return Analytic laplacian.
 */
template <typename vec_t>
typename vec_t::scalar_t u_laplacian(const vec_t& p, const mm::XML& conf) {
    if (conf.get<string>("case.name") == "exponential") {
        return u_laplacian(p, conf.get<double>("case.alpha"));
    } else if (conf.get<string>("case.name") == "log") {
        return u_laplacian(p);
    }
    assert_msg(false, "Unknown case name '%s'.", conf.get<string>("case.name"));
    throw std::runtime_error("Unknown case name.");
}

/**
 * Analytic gradient.
 * @tparam vec_t Vector.
 * @param p Position.
 * @param conf Configuration file.
 * @return Analytic gradient.
 */
template <typename vec_t>
vec_t u_gradient(const vec_t& p, const mm::XML& conf) {
    if (conf.get<string>("case.name") == "exponential") {
        return u_gradient(p, conf.get<double>("case.alpha"));
    } else if (conf.get<string>("case.name") == "log") {
        return u_gradient(p);
    }
    assert_msg(false, "Unknown case name '%s'.", conf.get<string>("case.name"));
    throw std::runtime_error("Unknown case name.");
}

#endif  // POISSON_HELPER_HPP
