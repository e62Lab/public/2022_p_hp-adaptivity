#include <medusa/Medusa_fwd.hpp>

#include "helpers/xml_helper.hpp"
#include "poisson/poisson_ball_cut.hpp"

using namespace std;
using namespace Eigen;
using namespace mm;

bool written = false;

template <typename vec_t, typename func_t>
void runSolve(const XML& conf, HDF& hdf, const BallShape<vec_t>& shape, const func_t& h, int i,
              Eigen::VectorXd& Ns, Eigen::VectorXd& times, Eigen::VectorXd& err1,
              Eigen::VectorXd& err2, Eigen::VectorXd& errinf, Eigen::VectorXd& err1_op,
              Eigen::VectorXd& err2_op, Eigen::VectorXd& errinf_op, Eigen::VectorXd& err1_imex,
              Eigen::VectorXd& err2_imex, Eigen::VectorXd& errinf_imex) {
    if (conf.get<int>("debug.print") == 1) {
        cout << "Solving case " << i << " ..." << endl;
    }
    // Initialize timer.
    Timer t;
    t.addCheckPoint("start");

    // Domain discretization.
    auto interior_type = conf.get<int>("domain.interior");
    auto boundary_type = conf.get<int>("domain.boundary");
    // Fill domain with nodes.
    DomainDiscretization<vec_t> domain = shape.discretizeBoundaryWithDensity(h, boundary_type);
    GeneralFill<vec_t> fill_randomized;
    int seed = conf.get<int>("domain.fill_seed");
    if (seed == -1) {
        seed = get_seed();
    }
    fill_randomized.numSamples(conf.get<int>("discretization.num_samples")).seed(seed);
    domain.addInternalNode(0.6, interior_type);
    fill_randomized(domain, h, interior_type);

    // Obtain and store domain size.
    int N = domain.size();
    Ns(i) = N;
    if (conf.get<int>("debug.print") == 1) {
        cout << "Domain size: " << N << "." << endl;
    }

    // Obtain implicit solution to the problem.
    Eigen::VectorXd rhs;
    VectorXd u = PoissonBallCut<vec_t>::solve(conf, h, domain, hdf, rhs);
    // Apply explicit operators to the implicitly obtained solution.
    VectorXd u_op = PoissonBallCut<vec_t>::apply_explicit_operators(conf, domain, hdf, u);

    t.addCheckPoint("end");

    // Error evaulation.
    Eigen::VectorXd sol_analytic(N);
    Eigen::VectorXd sol_operators_analytic(N);
    int type_interior = conf.get<int>("domain.interior");
    for (int k = 0; k < N; k++) {
        int type = domain.type(k);
        vec_t pos = domain.pos(k);

        // Analytic solution to compare with implicit.
        sol_analytic[k] = u_analytic(pos, conf);

        // Analytic operator field solution.
        if (type == type_interior) {
            sol_operators_analytic[k] = u_laplacian(pos, conf);
        } else if (pos[0] <= conf.get<double>("domain.neumann_threshold")) {
            // Dirichlet.
            sol_operators_analytic[k] = u_analytic(pos, conf);
        } else {
            vec_t normal = domain.normal(k);
            // Neumann.
            sol_operators_analytic[k] = normal.dot(u_gradient(pos, conf));
        }
    }

    // Errors.
    VectorXd err = sol_analytic - u;
    VectorXd err_op = sol_operators_analytic - u_op;
    VectorXd imex = (u_op - rhs).cwiseAbs();
    if (!written || conf.get<bool>("domain.write_all")) {
        // Write domain.
        written = true;
        // Add domain to output.
        hdf.atomic().writeDomain(format("domain_%.5i", i), domain);
        // Add solution to output.
        hdf.atomic().writeDoubleArray(format("domain_%.5i/sol", i), u);
        // Add explicit operator field to output.
        hdf.atomic().writeDoubleArray(format("domain_%.5i/sol_op", i), u_op);
        // Add imex to output.
        hdf.atomic().writeDoubleArray(format("domain_%.5i/imex", i), imex);
        // Add analytic error to output.
        hdf.atomic().writeDoubleArray(format("domain_%.5i/analytic_error_field", i), err);
    }

    // Error norms.
    // Solution error compared to analytic.
    err1(i) = err.lpNorm<1>() / sol_analytic.lpNorm<1>();
    err2(i) = err.lpNorm<2>() / sol_analytic.lpNorm<2>();
    errinf(i) = err.lpNorm<Eigen::Infinity>() / sol_analytic.lpNorm<Eigen::Infinity>();
    // Operator error compared to analytic.
    err1_op(i) = err_op.lpNorm<1>() / sol_operators_analytic.lpNorm<1>();
    err2_op(i) = err_op.lpNorm<2>() / sol_operators_analytic.lpNorm<2>();
    errinf_op(i) =
        err_op.lpNorm<Eigen::Infinity>() / sol_operators_analytic.lpNorm<Eigen::Infinity>();
    err1_imex = imex.lpNorm<1>();
    err2_imex = imex.lpNorm<2>();
    errinf_imex = imex.lpNorm<Eigen::Infinity>();

    // Execution times.
    times(i) = t.duration("start", "end");

    if (conf.get<int>("debug.print") == 1) {
        cout << "Finished solving case " << i << " ..." << endl;
    }
}

template <typename vec_t>
void runAll(const XML& conf, HDF& hdf) {
    // Get range of XMLs. (For scans over input parameters.)
    // Each XML is one computation.
    auto inputs = get_xml_range(conf);
    auto N_inputs = inputs.size();
    prn(N_inputs);  // Print number of cases to be computed.

    // Initialize vectors.
    Eigen::VectorXd NS(N_inputs);            // Number of nodes.
    Eigen::VectorXd TIMES(N_inputs);         // Total execution time for each case.
    Eigen::VectorXd ERR1(N_inputs);          // Error 1-norm.
    Eigen::VectorXd ERR2(N_inputs);          // Error 2-norm.
    Eigen::VectorXd ERRI(N_inputs);          // Error infinity-norm.
    Eigen::VectorXd ERR1_op(N_inputs);       // Error of explicit operators (imex), 1-norm.
    Eigen::VectorXd ERR2_op(N_inputs);       // Error of explicit operators (imex), 2-norm.
    Eigen::VectorXd ERRI_op(N_inputs);       // Error of explicit operators (imex), inifity-norm.
    Eigen::VectorXd ERR1_imex(N_inputs);     // Error of explicit operators (imex), 1-norm.
    Eigen::VectorXd ERR2_imex(N_inputs);     // Error of explicit operators (imex), 2-norm.
    Eigen::VectorXd ERRI_imex(N_inputs);     // Error of explicit operators (imex), inifity-norm.
    Range<string> configurations(N_inputs);  // Configuration - easier to read and plot.

    // Compute cases in parallel.
#pragma omp parallel for
    for (int i = 0; i < N_inputs; i++) {
        try {
            // Obtain current configuration.
            auto input = inputs[i];
            if (input.get<int>("debug.print") == 1) {
                prn(input);
            }

            // Domain shape.
            auto origin = input.get<double>("domain.origin");
            auto radius = input.get<double>("domain.radius");
            BallShape<vec_t> shape(origin, radius);

            // Node density function.
            auto dx = (2 * radius) / input.get<double>("discretization.nodes_per_side");
            auto fn = [=](const vec_t& p) { return dx; };

            // Run solve.
            runSolve(input, hdf, shape, fn, i, NS, TIMES, ERR1, ERR2, ERRI, ERR1_op, ERR2_op,
                     ERRI_op, ERR1_imex, ERR2_imex, ERRI_imex);

            // Obtain configuration params.
            auto basis_type = input.get<string>("approx.basis_type");
            auto mon_degree = input.get<int>("approx.mon_degree");
            auto nodes_per_side = input.get<int>("discretization.nodes_per_side");
            auto support_size = input.get<int>("approx.support_size");
            auto alpha = input.get<int>("case.alpha");
            auto case_name = input.get<string>("case.name");
            stringstream configuration;
            configuration << "case=" << case_name << ",basis=" << basis_type << ",m=" << mon_degree
                          << ",nps=" << nodes_per_side << ",idx=" << i
                          << ",support=" << support_size << ",alpha=" << alpha;
            // Add configuration to array.
            configurations[i] = configuration.str();
        } catch (const std::exception& e) {
            std::cout << " a standard exception was caught, with message '" << e.what() << "'\n";
        }
    }

    // Store to output.
    cout << "Results computed, saving to H5." << endl;
    hdf.reopen();
    hdf.openGroup("/");
    hdf.writeEigen("Ns", NS);
    hdf.writeEigen("err1", ERR1);
    hdf.writeEigen("err2", ERR2);
    hdf.writeEigen("errinf", ERRI);
    hdf.writeEigen("err1_op", ERR1_op);
    hdf.writeEigen("err2_op", ERR2_op);
    hdf.writeEigen("errinf_op", ERRI_op);
    hdf.writeEigen("err1_imex", ERR1_imex);
    hdf.writeEigen("err2_imex", ERR2_imex);
    hdf.writeEigen("errinf_imex", ERRI_imex);
    hdf.writeEigen("times", TIMES);
    for (int i = 0; i < N_inputs; i++) {
        hdf.writeStringAttribute(to_string(i), configurations[i]);
    }
    hdf.close();
}

int main(int argc, const char* argv[]) {
    // Check for settings input file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);
    // Set threads for parallel execution.
#if defined(_OPENMP)
    omp_set_num_threads(conf.get<int>("sys.threads"));
#endif
    // Create H5 hdf to store data..
    string output_hdf =
        conf.get<string>("meta.out_dir") + conf.get<string>("meta.out_file") + ".h5";
    cout << "Creating results hdf: " << output_hdf << endl;
    HDF hdf(output_hdf, HDF::DESTROY);

    // Write input to hdf.
    hdf.writeXML("conf", conf);
    hdf.close();

    // Compute.
    cout << "----------" << endl;
    cout << "Computing, please wait ..." << endl;
    int dim = conf.get<int>("domain.dim");
    switch (dim) {
        case 1:
            runAll<Vec1d>(conf, hdf);
            break;
        case 2:
            runAll<Vec2d>(conf, hdf);
            break;
        case 3:
            runAll<Vec3d>(conf, hdf);
            break;
        default:
            assert_msg(false, "Dimension %d not supported.", dim);
    }

    // End execution and write execution time.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_hdf << "." << endl;

    return 0;
}
