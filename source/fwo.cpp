#include <medusa/Medusa.hpp>
#include "fwo/fwo_hp_adaptive.h"

using namespace mm;
using namespace std;
using namespace Eigen;

int main(int argc, char* argv[]) {
    // Check for settings hdf.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Set num threads.
#if defined(_OPENMP)
    omp_set_num_threads(conf.get<int>("sys.threads"));
#endif

    // Create H5 hdf to store the parameters.
    string output_hdf =
        conf.get<string>("meta.out_dir") + conf.get<string>("meta.out_file") + ".h5";
    cout << "Creating results hdf: " << output_hdf << endl;
    HDF hdf(output_hdf, HDF::DESTROY);

    // Write params to results hdf.
    hdf.writeXML("conf", conf);

    cout << "----------" << endl;
    cout << "Computing, please wait ..." << endl;
    // Run hp-adaptive.
    run_fwo_hp_adaptive(conf, hdf);

    cout << "Calculations saved to: " << output_hdf << "." << endl;

    return 0;
}
