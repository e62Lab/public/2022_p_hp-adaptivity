echo 'Running for dim 1 ...'
taskset -c 0 ./fill_trumpet ../input/trumpet_dim1.xml
echo 'Running hp-adaptive for dim 1 ...'
taskset -c 0 ./conv_p_adaptive ../input/conv_adaptive_dim1.xml
echo 'Running h-adaptive for dim 1 ...'
taskset -c 0 ./conv_p_adaptive ../input/conv_adaptive_h_only_dim1.xml
echo 'Running p-adaptive for dim 1 ...'
taskset -c 0 ./conv_p_adaptive ../input/conv_adaptive_p_only_dim1.xml
echo 'All done.'
echo 'Running for dim 2 ...'
taskset -c 0 ./fill_trumpet ../input/trumpet_dim2.xml
echo 'Running hp-adaptive for dim 1 ...'
taskset -c 0 ./conv_p_adaptive ../input/conv_adaptive_dim2.xml
echo 'Running h-adaptive for dim 1 ...'
taskset -c 0 ./conv_p_adaptive ../input/conv_adaptive_h_only_dim2.xml
echo 'Running p-adaptive for dim 1 ...'
taskset -c 0 ./conv_p_adaptive ../input/conv_adaptive_p_only_dim2.xml
echo 'All done.'