import xml.etree.ElementTree as ET
import random

file_path = "../input/"
master_file = "poisson_dim2.xml"

# Read master file
tree = ET.parse(file_path + master_file)
root = tree.getroot()

# Manipulate XML and save
N = 10000
for i in range(N):
    h_delta1 = random.uniform(1e-4, 0.7)
    root.find("adaptivity/h").set("delta1", "{}".format(h_delta1))


    h_delta2 = random.uniform(h_delta1, 0.9)
    root.find("adaptivity/h").set("delta2", "{}".format(h_delta2))

    h_refine = random.uniform(1.5, 6)
    root.find("adaptivity/h").set("agressiveness", "{}".format(h_refine))

    h_deref = random.uniform(1.01, 2)
    root.find("adaptivity/derefine/h").set("agressiveness", "{}".format(h_deref))

    # General.
    root.find("meta").set(
        'out_file', 'results_%.6d' % i)
    tree.write(
        '../input/generated/settings_%.6d.xml' % i)

print('Created ' + str(N) + ' files.')
