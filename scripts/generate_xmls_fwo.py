import xml.etree.ElementTree as ET
import random

file_path = "../input/"
master_file = "fwo.xml"

# Read master file
tree = ET.parse(file_path + master_file)
root = tree.getroot()

# Manipulated data
N = 10000

# Manipulate XML and save
for i in range(N):
    # H
    delta_1 = random.uniform(0.001, 0.1)
    root.find("adaptivity/h").set("delta1", "{}".format(delta_1))
    delta_2 = random.uniform(0.002, 0.3)
    root.find("adaptivity/h").set("delta2", "{}".format(delta_2))
    ref_aggr = random.uniform(1.5, 6)
    root.find("adaptivity/h").set("agressiveness", "{}".format(ref_aggr))
    deref_aggr = random.uniform(1.01, 3)
    root.find("adaptivity/derefine/h").set("agressiveness", "{}".format(deref_aggr))
    # P
    delta_1 = random.uniform(0.0001, 0.1)
    root.find("adaptivity/p").set("delta1", "{}".format(delta_1))
    delta_2 = random.uniform(0.01, 0.9)
    root.find("adaptivity/p").set("delta2", "{}".format(delta_2))
    ref_aggr = random.uniform(1.5, 4)
    root.find("adaptivity/p").set("agressiveness", "{}".format(ref_aggr))
    deref_aggr = random.uniform(1.01, 3)
    root.find("adaptivity/derefine/p").set("agressiveness", "{}".format(deref_aggr))
    # General.
    root.find("meta").set('out_file', 'results_%.6d' % i)
    tree.write('../input/generated_fwo/settings_%.6d.xml' % i)

print('Created ' + str(N) + ' files.')
