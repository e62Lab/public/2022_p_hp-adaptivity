# Implementation of $`hp`$-adaptive solution procedure

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/e62Lab%2Fpublic%2F2022_p_hp-adaptivity/master)

Implementation of $`hp`$-adaptive solution procedure in the context of meshless methods.

## Description

$`p`$-adaptivity is where the order of the approximation method spatially varies throughout the
domain while $`h`$-adaptivity is where the nodal distribution is spatially variable. Our goal is to combine both.

## Visuals

Check the `root/results/` directory for any visuals.

## Installation

Requirements:

- CMake
- C++ compiler
- Python 3.9 (or higher)
- Jupyter notebooks
- Medusa [see here](https://e6.ijs.si/medusa/wiki/index.php/Medusa)

## Usage

Create or go to `root/build/` directory and build using

```bash
cmake .. && make -j 12
```

The executables will be created in `root/bin/` directory. The executable must be run with a parameter
with all the settings, e.g.

```bash
./adaptive ../input/settings.xml
```

## Support

Thanks to [E62 team](https://e6.ijs.si/parlab/) for support.

## Contributing

Entire [E62 team](https://e6.ijs.si/parlab/).

## Authors and contributors

Mitja Jančič under supervision and guidance of Gregor Kosec. Both members of the [E62 team](https://e6.ijs.si/parlab/).

## Thanks

Thanks to Miha Rot for fruitful discussions.

## License

This project is open source and free to use.

# Reproducing results

## RBF-FD convergence rates

To reproduce RBF-FD convergence rates with different monomials orders, run

```bash
./fill_trumpet ../input/trumpet_dim1.xml
```

## $`hp`$-adaptive solution to Poisson problem with exponentially strong source

To reproduce $`hp`$-adaptive solution to Poisson problem with exponentially strong source, run

```bash
./poisson ../input/poisson_dim2.xml
```

## Fretting fatigue problem

To reproduce the fretting fatigue problem, run

```bash
./fwo ../input/fwo.xml
```

## Three dimensional Boussinesq's problem

To reproduce the results from three-dimensional Boussinesq's problem, run

```bash
./boussinesq ../input/boussines.xml
```